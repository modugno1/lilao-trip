<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Para testes
 */
Route::get('/teste', function() {
	// teste
});

/**
 * Site
 */
Route::get('/', 'SiteController@home');
// Route::get('novo-site', 'SiteController@home');
Route::get('viagem/{slug}', 'SiteController@viagem');
Route::get('enviado/contato', 'SiteController@contatoEnviado');
Route::post('contato/enviar', 'SiteController@enviarContato');

// blog
Route::get('blog', 'BlogController@blog')->name('blogsite');
Route::get('blog/{slug}', 'BlogController@post')->name('blogsite.post');

/**
 * Site em construção
 */
Route::get('manutencao', 'ManutencaoController@construcao');
// Route::get('/', 'ManutencaoController@construcao');

// Log Viewer
Route::group(['middleware' => 'auth'], function() {
	Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

/**
 * Login | Registros | Recuperação de Senha
 */
Route::group(['namespace' => 'login'], function() {

	Route::get('login', 'AuthenticateController@login');
	Route::get('logout', 'AuthenticateController@logout');
	Route::post('auth', 'AuthenticateController@auth');
	Route::post('auth/email', 'AuthenticateController@checkEmail');
});

/**
 * Painel Administrativo do Fragment
 */
Route::group(['prefix' => 'admin', 'namespace' => 'Dashboard'], function() {

	// dashboard
	Route::get('dashboard', 'DashboardController@index');
	Route::get('manutencao', 'DashboardController@manutencao');
	Route::post('manutencao/cadastrar', 'DashboardController@cadastrarManutencao');

	// usuario
	Route::get('usuarios', 'UsuarioController@index');
	Route::get('usuario/editar/{id}', 'UsuarioController@editar');
	Route::post('usuario/atualizar', 'UsuarioController@atualizar');
	
	// viagem
	Route::get('viagem', 'ViagemController@index');
	Route::get('viagem/novo', 'ViagemController@novo');
	Route::post('viagem/cadastrar', 'ViagemController@cadastrar');
	Route::get('viagem/editar/{id}', 'ViagemController@editar');
	Route::post('viagem/atualizar', 'ViagemController@atualizar');
	Route::get('viagem/remover/{id}', 'ViagemController@remover');
	Route::get('viagem/{id}/imagem', 'ViagemController@addImagem');
	Route::post('viagem/imagem/atualizar', 'ViagemController@atualizarImagem');

	// estadia
	Route::get('estadia/{viagem_id}', 'EstadiaController@index');
	Route::get('estadia/novo/{viagem_id}', 'EstadiaController@novo');
	Route::post('estadia/cadastrar', 'EstadiaController@cadastrar');
	Route::get('estadia/editar/{id}', 'EstadiaController@editar');
	Route::post('estadia/atualizar', 'EstadiaController@atualizar');
	Route::get('estadia/remover/{id}', 'EstadiaController@remover');

	// cronograma
	Route::get('cronograma/{viagem_id}', 'CronogramaController@index');
	Route::get('cronograma/novo/{viagem_id}', 'CronogramaController@novo');
	Route::post('cronograma/cadastrar', 'CronogramaController@cadastrar');
	Route::get('cronograma/editar/{id}', 'CronogramaController@editar');
	Route::post('cronograma/atualizar', 'CronogramaController@atualizar');
	Route::get('cronograma/remover/{id}', 'CronogramaController@remover');

	// banner
	Route::get('banner', 'BannerController@index')->name('banner');
	Route::get('banner/novo', 'BannerController@novo')->name('banner.new');
	Route::post('banner', 'BannerController@cadastrar')->name('banner.create');
	Route::get('banner/{id}/editar', 'BannerController@editar')->name('banner.edit');
	Route::post('banner/{id}', 'BannerController@atualizar')->name('banner.update');
	Route::get('banner/{id}', 'BannerController@remover')->name('banner.remove');
	Route::get('banner/{id}/imagem', 'BannerController@addImagem')->name('banner.image');
	Route::post('banner/imagem/atualizar', 'BannerController@atualizarImagem')->name('banner.atualizarimage');

	// photos
	Route::get('photo', 'PhotosController@index')->name('photo');
	Route::get('photo/novo', 'PhotosController@novo')->name('photo.new');
	Route::post('photo/cadastrar', 'PhotosController@cadastrar')->name('photo.create');
	Route::get('photo/{id}', 'PhotosController@remover')->name('photo.remove');

	// post
	Route::get('post', 'PostController@index')->name('post');
	Route::get('post/novo', 'PostController@novo')->name('post.new');
	Route::post('post', 'PostController@cadastrar')->name('post.create');
	Route::get('post/{id}/editar', 'PostController@editar')->name('post.edit');
	Route::post('post/{id}', 'PostController@atualizar')->name('post.update');
	Route::get('post/{id}', 'PostController@remover')->name('post.remove');

	// depoimentos
	Route::get('depoimento', 'DepoimentoController@index')->name('depoimento');
	Route::get('depoimento/novo', 'DepoimentoController@novo')->name('depoimento.new');
	Route::post('depoimento', 'DepoimentoController@cadastrar')->name('depoimento.create');
	Route::get('depoimento/{id}/editar', 'DepoimentoController@editar')->name('depoimento.edit');
	Route::post('depoimento/{id}', 'DepoimentoController@atualizar')->name('depoimento.update');
	Route::get('depoimento/{id}', 'DepoimentoController@remover')->name('depoimento.remove');

	// sobre
	Route::get('quem-somos/editar', 'AboutController@editar')->name('about.edit');
	Route::post('quem-somos/{id}', 'AboutController@atualizar')->name('about.update');
});

// api
Route::group(['prefix' => 'api'], function() {
	
	// posts
	Route::get('posts', 'BlogController@getAll');
});