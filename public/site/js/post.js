const post = (() => {

    function init() {
        
        $('form#search').on('submit', function(e) {
            e.preventDefault();
            window.location.href = `/blog?s=${ $('#search_input').val() }#posts`;
        });
    }

    return { init }
})();

post.init();