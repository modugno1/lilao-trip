$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
const blog = (() => {

    const endpoint = 'http://lilaotrip.com.br/api';
    var posts = [];
    let page = 1,
        perPage = 5,
        s;

    function get() {
        return $.ajax(url);
    }

    function getPosts(s, loadmore = false) {
        if (!loadmore) {
            loader(true);
        }
        const search = (s) ? `&s=${s}` : '';
        const url = `${endpoint}/posts?page=${page}&perPage=${perPage}${search}`;
        return $.ajax(url).then(res => fetchData(res, search), handleError);
    }

    function render(posts) {

        if (!posts.length) {
            return `<h1 class='text-center'>Nenhum post encontrado =[</h1>`;
        }
        return posts.map(post => {
            return `
            <article class="l-post__item">
                <header class="l-post__header">
                    <h2 class="m-post__title">
                        <a href="/blog/${post.slug}#posts">${post.title}</a>
                    </h2>
                    <time class="m-post__date">
                        Publicado em ${post.createdAt} - <span>Última atualização ${post.updatedAt}</span>
                    </time>
                    <span class="m-post__views text-muted">${post.views} Visualizações</span> 
                </header>
                <a href="/blog/${post.slug}#posts">
                    <img src="/upload/post/${post.image}" alt="${post.title}" class="img-responsive m-post__thumb">
                </a>
                <hr>
                <p class="m-post__content lead">
                    ${post.excerpt}
                </p>
                <p class="m-post__btn-read-more text-right">
                    <a href="/blog/${post.slug}#posts">Leia Mais</a> 
                </p>
            </article>  
            `;
        }).join('');
    }

    function loader(flag) {
        const loading = $('.loading');
        if (flag) {
            $('#post-container').children().remove();
            return loading.addClass('active');
        }

        loading.removeClass('active');
    }

    function fetchData(response, search) {
        blockbutton(false);
        if (!search) {
            for (res of response) {
                posts.push(res);
            }

        } else {
            posts = response;
        }
        
        const markup = render(posts);
        document.getElementById('post-container').innerHTML = markup;
        loader(false);
    }

    function blockbutton(flag) {
        const text = (flag) ? 'Carregando...' : 'Carregar mais';
        $('#loadmore').text(text);
        if (flag) {
            $('#loadmore').attr('disabled', 'disabled');
        } else {
            $('#loadmore').removeAttr('disabled');
        }
        
    }

    function handleError(error) {
        console.log('ERROR', error);
    }

    function init() {

        const queryString = $('#searchQuery').val();
        getPosts( queryString );

        // when typed input
        $('#search_input').on('input', function(e) {
            const s = $(this).val();
            const title = 'Testando';
            const url = `/blog?s=${s}`;
            window.history.pushState({ s }, title, url);
        });

        // when search submit
        $('form#search').on('submit', function(e) {
            e.preventDefault();
            
            const s = $('#search_input').val();
            perPage = (s) ? 1000 : 5;
            page = 1;
            getPosts(s);

            // hide load more button when on search
            if (s) {
                $('#loadmore').hide();
            } else {
                $('#loadmore').show();
            }
            
        });

        // when load more was clicked
        $('#loadmore').on('click', function(e) {
            e.preventDefault();
            blockbutton(true);
            perPage = 5;
            page++;
            getPosts(null, true);
        })
    }
    return { init }
})();

blog.init();