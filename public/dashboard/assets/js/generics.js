
/**
 * Desabilita o botão de load
 * @return {[type]} [description]
 */
var desativaBtnLoad = function(botao, texto) {
	botao.find('span').text(texto);	
	botao.prop('disabled', true);
};

/**
 * Mostra a imagem de load do botão
 * @return {[type]} [description]
 */
var showImgBtn = function() {
	$('.btn-load-img').show();
};

/**
 * Esconde a imagem de load do botão
 * @return {[type]} [description]
 */
var hideImgBtn = function() {
	$('.btn-load-img').hide();
};
	
/**
 * Método para redirecionar o usuário
 * @param  {[type]} target caminho
 * @return {[type]}        [description]
 */
var redirect = function(target) {
	window.location.href = target;
};