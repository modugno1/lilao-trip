$(document).ready(function() {

	// campos ocultos
	let $response = $('.response');
	
	// campos do formulário
	let $email    = $('#email');
	let $password = $('#password');
	let $btnLogin = $('.btn-login');
	let $form     = $('#form-login');

	// esconde a imagem de load
	hideImgBtn();

	// Antes de enviar o formulário, valida se o e-mail existe
	$form.on('submit', function(e) {
		e.preventDefault();

		$response.html("");
		showImgBtn();
		desativaBtnLoad($btnLogin, 'Autenticando');

		// faz a requisição
		$.post('/auth', { data: $(this).serialize() }, function(response) {
			
			if (!response.status) {
				$btnLogin.find('span').text('Entrar');
				$btnLogin.prop('disabled', false);
				hideImgBtn();
				$response.html(`<p class='alert alert-danger'>${response.msg}</p>`);
				return;
			}

			$response.html(`<p class='alert alert-success'>${response.msg}</p>`);
			redirect('/admin/viagem');

		}, 'json');
		
	});
});