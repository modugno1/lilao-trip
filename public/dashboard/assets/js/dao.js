/**
 * Biliotecas de DAO
 * @param  {[type]} ) {}          [description]
 * @return {[type]}   [description]
 */

var get = function(target) {
	$.ajax({
		method: 'GET',
		url: target,
		dataType: 'jsonp'
	})
	.done(function(response) {
		return response;
	})
	.fail(function(error) {
		return error;
	});
};

$.extend({
	
	// Post
	xPost: function(target, data) {
		// get response
		var objResponse = null;
		// do request
		$.ajax({
			method: 'POST',
			url: target,
			dataType: 'json',
			data: data,	
			async: false,
			success: function(response) {
				objResponse = response;
			}
		});

		// return data
		return objResponse;
	}

});
