$(document).ready(function() {

	/**
	 * Configurações Padrão
	 */
	
	// esconde a imagem de load
	hideImgBtn();

	/** Envio normal, com refresh. Bloqueia o botão de salvar ao submeter o formulário */
	$('#form-submit').on('submit', function() {
		let $btnSave = $('.btn-save');
		showImgBtn(); // mostra imagem de load
		desativaBtnLoad($btnSave, 'Salvando'); // desativa o botão e troca o texto
		$(this).submit();
	});

	// ações de quando os botões de "salvar" for clicado
	$('.btn-remover').on('click', function() {
		desativaBtnLoad($(this)); // desativa o botão e troca o texto
	});
});