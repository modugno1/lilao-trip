$(document).ready(function() {
	// mostra a senha
	$('.mostrar-senha').on('change', function() {
		// verifica se esta checado, e muda o campo 'password' para 'text'
		if ($(this).is(':checked')) {
			$(this).parents('.form-group').find('.password').attr('type', 'text');
		} else {
			$(this).parents('.form-group').find('.password').attr('type', 'password');
		}
	});

	// btn remover
	var id;
	$('.remover').on('click', function() {
		id = $(this).data('id');
	});

	// quando o modal 'remover' abrir
	$('#remover').on('show.bs.modal', function(e) {
		// atribui o id a ser removido, ao campo 'id' do modal
		$(this).find('#id').val(id);
	});
});