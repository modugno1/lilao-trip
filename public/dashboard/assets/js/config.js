/**
 * Configurações do Sistema
 */

$(document).ready(function() {

	/**
	 * Seta o cabeçalho de todas as requisições ajax, para mandar o Token de segurança CSRF para os formulários
	 * @type {Object}
	 */
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

});