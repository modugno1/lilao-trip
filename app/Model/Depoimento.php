<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Depoimento extends Model
{
    protected $table      = 'depoimento';
    protected $primaryKey = 'id';
    protected $guarded    = ['id'];
}
