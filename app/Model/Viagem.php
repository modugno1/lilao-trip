<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Viagem extends Model
{
    protected $table      = 'viagem';
    protected $primaryKey = 'id';
    protected $guarded    = ['id'];
    public    $timestamps = false;

    /**
     * Retorna todas as estadias da viagem
     * @return [type] [description]
     */
    public function estadias()
    {
    	return $this->hasMany('App\Model\Estadia', 'viagem_id');
    }

    /**
     * Retorna todas os cronogramas da viagem
     * @return [type] [description]
     */
    public function cronogramas()
    {
    	return $this->hasMany('App\Model\Cronograma', 'viagem_id');
    }
}
