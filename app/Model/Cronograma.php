<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cronograma extends Model
{
    protected $table      = 'cronograma';
    protected $primaryKey = 'id';
    protected $guarded    = ['id'];
    public    $timestamps = false;

    /**
     * Retorna a viagem pai
     * @return [type] [description]
     */
    public function viagem()
    {
    	return $this->belongsTo('App\Model\Viagem');
    }
}
