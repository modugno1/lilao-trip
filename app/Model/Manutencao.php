<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Manutencao extends Model
{
    protected $table      = 'manutencao';
    protected $primaryKey = 'id';
    protected $guarded    = ['id'];
    public    $timestamps = false;
}
