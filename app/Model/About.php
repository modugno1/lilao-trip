<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table      = 'about';
    protected $primaryKey = 'id';
    protected $guarded    = ['id'];
    public    $timestamps = false;
}
