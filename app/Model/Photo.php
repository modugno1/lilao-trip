<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table      = 'photo';
    protected $primaryKey = 'id';
    protected $guarded    = ['id'];
    public    $timestamps = false;
}
