<?php

/**
 * Arquivo para registrar os helpers da aplicação
 * Seguir sempre o padrão do laravel como as demais funções abaixo
 */

if (!function_exists('str_initial')) {

	/**
	 * Gera Iniciais de uma String: ex Nome Sobrenome = N S
	 * @param  String $str  string para ser formatada
	 * @return String string formatada
	 */
	function str_initial($str, $separator = null, $qtde = true)
	{
		// separa o nome e sobrenome caso tenha
		$array = explode(' ', $str);

		// caso não tenha nome e sobrenome
		if (count($array) < 2) {
			$str_len = strlen($str) - 1;
			return substr($str, 0, -$str_len);
		}

		// se tiver nome e sobrenome gera iniciais
		$primeiraLetra = substr($array[0], 0, 1);
		$segundaLetra  = substr($array[1], 0, 1);

		// retorna as letras
		$separator = ($separator !== null) ? $separator : ' ';

		// se for falso, mesmo que ele tenha sobrenome, retorna só a primeira letra
		if (!$qtde) {
			return sprintf("%s", $primeiraLetra);
		}
		
		return sprintf("%s{$separator}%s", $primeiraLetra, $segundaLetra);

	}
}

if (!function_exists('str_password')) {
	/**
	 * Gera Senha aleatória
	 * @param  int $qtde_caracteres Quantidade de Caracteres
	 * @return String Senha
	 */
	function str_password($qtde_caracteres = 6)
	{
		$random = microtime(); // gera número aleatorio
		$encrypt = md5($random); // encriptografa

		return substr($encrypt, 0, $qtde_caracteres); // retorna a senha
	}
}

/**
 * Formulario de upload
 */
if (!function_exists('upload')) {

	function upload($file, $folder = null)
	{
		// trata o path onde sera salvo a imagem
		$path = 'upload/banners/';
		if (!empty($folder)) {
			$path = $folder;
		}

		// se o diretorio não existir, cria
		if (!is_dir($path)) {
			mkdir($path, 0777, true);
		}

		$nome   = $file->getClientOriginalName(); // pega o nome original
		$nome   = md5($nome . date('His')); // encriptografa o nome contatenando com a hora atual
		$ext    = $file->getClientOriginalExtension(); // pega a extensão do arquivo
		$imagem = sprintf('%s.%s', $nome, $ext);
		$file->move($path, $imagem);

		return $imagem;
	}
}

if (!function_exists('timestamp')) {
	function timestamp($date, $formatZone = 'br') {

		// formatos para as datas
		$formatTypes = ['br', 'BR', 'eua', 'EUA'];

		if (!in_array($formatZone, $formatTypes)) {
			throw new \InvalidArgumentException(sprintf("'%s' não é formato válido.", $formatZone));
		}

		// valida o formato americano ou brasileiro
		if (strtoupper($formatZone) == 'EUA') {
			$format = 'Y-m-d H:i:s';
		} elseif (strtoupper($formatZone) == 'BR') {
			$format = 'd/m/Y H:i:s';
		}

        return \Carbon\Carbon::createFromFormat($format, $date);
	}
}

if (!function_exists('dateformat_eua')) {
	function dateformat_eua($date, $format = 'd/m/Y') {
        if (empty($date)) {
            return;
        }
        
		return \Carbon\Carbon::createFromFormat($format, $date)->format('Y-m-d');
	}
}

if (!function_exists('dateformat_br')) {
	function dateformat_br($date, $format = 'Y-m-d') {
        if (empty($date)) {
            return;
        }

        return \Carbon\Carbon::createFromFormat($format, $date)->format('d/m/Y');
	}
}
