<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'     => 'required|min:2|max:80',
            'email'    => 'required|min:6|max:150|email|unique:users,email,' . $this->id,
        ];

        // se não for atualização, campo senha é obrigatório
        if (!isset($this->id)) {
            $rules['password'] = 'required|min:6|max:12';
        }

        // retorna as regras
        return $rules;
    }

    /**
     * Mensagens dos campos
     * @return [type] [description]
     */
    public function messages()
    {

        $messages = [
            // nome
            'name.required' => 'Informe o campo Nome',
            'name.min' => 'O campo Nome deve ter no mínimo :min caracteres',
            'name.max' => 'O campo Nome deve ter no máximo :max caracteres',

            // email
            'email.required' => 'Informe o campo E-mail',
            'email.email' => 'Informe um E-mail válido',
            'email.unique' => 'O E-mail que você tentou cadastrar, ja existe em nosso sistema, tente outro.',
            'email.min' => 'O campo E-mail deve ter no mínimo :min caracteres',
            'email.max' => 'O campo E-mail deve ter no máximo :max caracteres',
        ];

        // se não for atualização
        if (!isset($this->id)) {
            // password
            $messages['password.required'] = 'Informe o campo Senha';
            $messages['password.min']      = 'O campo Senha deve ter no mínimo :min caracteres';
            $messages['password.max']      = 'O campo Senha deve ter no máximo :max caracteres';
        }

        // retorna as mensagens
        return $messages;
    }
}
