<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Manutencao;
class Producao
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // verifica o ambiente, se é produção ou homologação
        $manutencao = Manutencao::find(1);
        if ($manutencao->manutencao == 1) {
            return redirect('/manutencao');
        }

        return $next($request);
    }
}
