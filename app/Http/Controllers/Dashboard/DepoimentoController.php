<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session;
use DB;
use Log;
use App\Model\Depoimento;

class DepoimentoController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	$dados['depoimentos'] = Depoimento::orderBy('created_at', 'desc')->paginate(10);
    	return view('dashboard.depoimento.listar', $dados);
    }

    public function novo()
    {
    	return view('dashboard.depoimento.novo');
    }

    public function cadastrar(Request $request)
    {
    	try {

    		DB::beginTransaction();
    		
            $depo = new Depoimento;
            $depo->name = $request->input('name');
            $depo->content = $request->input('content');
            $depo->date = $request->input('date');
    		$depo->save();
    		
    		DB::commit();

            return redirect()->route('depoimento')->with('msg-ok', 'Depoimento Cadastrado com sucesso.');

    	} catch(\Exception $e) {
    		DB::rollback();
    		Log::error('DepoimentoController@cadastrar: ' . $e->getMessage());
    		return redirect()->back()->withInput()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
    	}
    }

    public function editar($id)
    {
    	try {
    		$dados['depoimento'] = Depoimento::findOrFail($id);
    		return view('dashboard.depoimento.editar', $dados);
    	} catch(\Exception $e) {
    		Log::error('DepoimentoController@editar: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Depoimento não encontrado com o ID #' . $id);
    	}
    }

    public function atualizar(Request $request, $id)
    {
        
        try {

            DB::beginTransaction();
            
            $depo = Depoimento::findOrFail($id);
            $depo->name = $request->input('name');
            $depo->content = $request->input('content');
            $depo->date = $request->input('date');
            $depo->save();
            DB::commit();

            return redirect()->back()->with('msg-ok', 'Depoimento Atualizado com sucesso.');

        } catch(\Exception $e) {
            DB::rollback();
            Log::error('DepoimentoController@atualizar: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
        }
    }

    public function remover($id)
    {
    	try {
    		$depo = Depoimento::findOrFail($id);
    	} catch(\Exception $e) {
    		Log::error('DepoimentoController@remover: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Depoimento não encontrado com o ID #' . $id);
    	}

        try {
            $depo->delete();
            return redirect()->back()->with('msg-ok', 'Depoimento removido com sucesso.');
        } catch(\Exception $e) {
            Log::error('DepoimentoController@remover: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro ao remover o Depoimento, tente novamente.');
        }

    }    
}
