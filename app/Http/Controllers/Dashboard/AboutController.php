<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session;
use DB;
use Log;
use App\Model\About;

class AboutController extends Controller
{

    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function editar()
    {
    	try {
            $dados['about'] = About::first();
    		return view('dashboard.about.editar', $dados);
    	} catch(\Exception $e) {
    		Log::error('AboutController@editar: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Sobre não encontrado');
    	}
    }

    public function atualizar(Request $request, $id)
    {
        
        try {

            DB::beginTransaction();
            
            $about = About::findOrFail($id);
            $about->title_about = $request->input('title_about');
            $about->content_about = $request->input('content_about');
            $about->title_why = $request->input('title_why');
            $about->content_why = $request->input('content_why');
            $about->video_one = $request->input('video_one');
            $about->video_two = $request->input('video_two');
            $about->save();
            DB::commit();

            return redirect()->back()->with('msg-ok', 'Quem Somos Atualizado com sucesso.');

        } catch(\Exception $e) {
            DB::rollback();
            Log::error('AboutController@atualizar: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
        }
    }
}
