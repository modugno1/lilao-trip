<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsuarioRequest;
use App\User;
use Cache;
use DB;
use Illuminate\Http\Request;
use Log;
use Hash;
use Session;

class UsuarioController extends Controller
{
	
	public function __construct()
	{
		$this->middleware('auth');
	}   

	/**
	 * Retorna a Lista de Usuários
	 * @return [type] [description]
	 */
	public function index(Request $request)
	{
		$schema = User::select('id', 'name', 'email')->orderBy('name');


		// se for pesquisa, faz a consulta a retorna os usuarios
		if ($request->input('pesquisar')) {

			$dados['usuarios'] = $schema->where(function($q) use ($request) {
											$q->where("name", "LIKE", "%{$request->input('pesquisar')}%");
			                            	$q->orWhere("email", "LIKE", "%{$request->input('pesquisar')}%");
										})->paginate(15);

			// num. de resultados da pesquisa
			$dados['num_pesquisa'] = count($dados['usuarios']);
			$dados['pesquisa']     = $request->input('pesquisar');

			// Monta filtros, para páginação
			$dados['filtros']['pesquisar'] = $request->input('pesquisar');

		} else {
			// se não, retorna os usuários normalmente
			$dados['usuarios'] = $schema->paginate(15);
		}

		return view('dashboard.usuario.listar', $dados);
	}

	/**
	 * Retorna o resultado das pesquisas
	 * @return [type] [description]
	 */


	/**
	 * Exibe a tela de cadastro
	 * @return [type] [description]
	 */
	public function novo()
	{
		// URL do Form
		$dados['form_url'] = 'fragment/usuario/cadastrar';
		return view('dashboard.usuario.form', $dados);
	}

	/**
	 * Exibe a tela de edição
	 * @param  [type] $id ID do Usuário
	 * @return [type]     [description]
	 */
	public function editar($id)
	{
		try {
			// dados do usuário
			$dados['user'] = User::findOrFail($id);
			// URL do Form
			$dados['form_url'] = 'admin/usuario/atualizar';

			return view('dashboard.usuario.form', $dados);	

		} catch(\Exception $e) {
			Log::error('Erro ao encontrar usuário com o ID ' . $id . ' - UsuarioController@editar: ' . $e->getMessage());
			return redirect()->back()->with('msg-error', 'Não foi possível encontrar o usuário com o ID ' . $id);
		}
	}

	/**
	 * Cadastra o usuário
	 * @return [type] [description]
	 */
	public function cadastrar(UsuarioRequest $request)
	{
		try {
			User::create($request->all());

			return redirect('/fragment/usuarios')->with('msg-ok', 'Usuário cadastrado com sucesso.');

		} catch(\Exception $e) {
			Log::error('Erro ao cadastrar usuario - UsuarioController@cadastrar: ' . $e->getMessage());
			return redirect()->back()->with('msg-error', 'Não foi possível cadastrar o usuário, contate o suporte.');
		}
	}

	/**
	 * Atualiza o usuário
	 * @return [type] [description]
	 */
	public function atualizar(UsuarioRequest $request)
	{
		// ID do usuário
		$id = $request->input('id');

		try {

			$user = User::findOrFail($id);
			$user->fill($request->except(['password_old', 'password_new']));

			if ($request->input('password_old') && $request->input('password_new')) {

				// valida se a senha antiga é igual a senha cadastrada
				if (!Hash::check($request->input('password_old'), $user->password)) {
					return redirect()->back()->with('msg-error', 'A Senha Antiga não bate com a senha atual.');
				} 

				// seta a nova senha
				$user->password = $request->input('password_new');
			}

			$user->save();

			return redirect('/admin/usuario/editar/' . $id)->with('msg-ok', 'Usuário atualizado com sucesso.');

		} catch(\Exception $e) {
			Log::error('Erro ao atualizar usuario - UsuarioController@atualizar: ' . $e->getMessage());
			return redirect()->back()->with('msg-error', 'Não foi possível atualizar o usuário, contate o suporte.');
		}
	}

	/**
	 * Remove o usuário
	 * @return [type] [description]
	 */
	public function remover(Request $request)
	{
		try {

			$user = User::findOrFail($request->input('id'));
			$user->delete();
			return redirect('/fragment/usuarios')->with('msg-ok', 'Usuário removido com sucesso.');

		} catch(\Exception $e) {
			Log::error('Erro ao encontrar o ID para remover - UsuarioController@remover: ' . $e->getMessage());
			return redirect()->back()->with('msg-error', 'Não foi possível remover o registro, contate o suporte.');
		}
	}
}
