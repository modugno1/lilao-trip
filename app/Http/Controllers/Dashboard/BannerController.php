<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Log;
use App\Model\Banner;

class BannerController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	$dados['banners'] = Banner::orderBy('id', 'desc')->get();
    	return view('dashboard.banner.listar', $dados);
    }

    public function novo()
    {
    	return view('dashboard.banner.novo');
    }

    public function cadastrar(Request $request)
    {
    	
    	try {

    		DB::beginTransaction();
    		
    		$banner = new Banner;
            $banner->description = $request->input('description');
            
            if ($request->hasFile('image')) {
                $banner->image = upload($request->file('image'), 'upload/banner');
            }

    		$banner->save();
    		
    		DB::commit();

            return redirect()->route('banner')->with('msg-ok', 'Banner Cadastrado com sucesso.');

    	} catch(\Exception $e) {
    		DB::rollback();
    		Log::error('BannerController@cadastrar: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
    	}
    }

    public function editar($id)
    {
    	try {
    		$dados['banner'] = Banner::findOrFail($id);
    		return view('dashboard.banner.editar', $dados);
    	} catch(\Exception $e) {
    		Log::error('BannerController@editar: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Banner não encontrado com o ID #' . $id);
    	}
    }

    public function atualizar(Request $request, $id)
    {
        
        try {

            DB::beginTransaction();
            
            $banner = Banner::findOrFail($id);
            $banner->description = $request->input('description');            
            $banner->save();
            DB::commit();

            return redirect()->back()->with('msg-ok', 'Banner Atualizado com sucesso.');

        } catch(\Exception $e) {
            DB::rollback();
            Log::error('BannerController@atualizar: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
        }
    }

    public function remover($id)
    {
    	try {
    		$banner = Banner::findOrFail($id);
    	} catch(\Exception $e) {
    		Log::error('BannerController@remover: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Banner não encontrado com o ID #' . $id);
    	}

        try {
            $banner->delete();
            return redirect()->route('banner')->with('msg-ok', 'Banner removido com sucesso.');
        } catch(\Exception $e) {
            Log::error('BannerController@remover: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro ao remover os banners, tente novamente.');
        }

    }

    public function addImagem($banner_id)
    {
        $dados['banner'] = Banner::find($banner_id);
        return view('dashboard.banner.add-imagem', $dados);
    }

    public function atualizarImagem(Request $request)
    {
        $banner = Banner::find($request->input('banner_id'));

        if (!$request->hasFile('image')) {
            return redirect()->back()->with('msg-warning', 'Nenhuma imagem selecionada.');
        }

        try {
            $imagem = upload($request->file('image'), 'upload/banner');
            $banner->image = $imagem;
            $banner->save();
            
            return redirect()->back()->with('msg-ok', 'Imagem Cadastrada com sucesso.');
        } catch(\Exception $e) {    
            \Log::error('BannerController@atualizarImagem: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro ao cadastrar a imagem.');
        }
        
    }
}
