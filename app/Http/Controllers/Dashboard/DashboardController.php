<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Manutencao;
use Session;

class DashboardController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	return view('dashboard.default.tabela');
    }

    public function manutencao()
    {
    	$dados['manutencao'] = Manutencao::find(1);
    	return view('dashboard.home.manutencao', $dados);
    }

    public function cadastrarManutencao(Request $request)
    {
    	try {
    		$manutencao = Manutencao::find(1);
	    	$manutencao->manutencao = $request->input('manutencao');
	    	$manutencao->save();

	    	return redirect()->back()->with('msg-ok', 'Ambiente foi alterado com sucesso.');
    	} catch(\Exception $e) {
    		return redirect()->back()->with('msg-error', 'Ocorreu um erro ao mudar o ambiente, tente novamente.');
    	}

    }
}
