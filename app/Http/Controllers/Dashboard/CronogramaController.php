<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use DB;
use Log;
use App\Model\Cronograma;

class CronogramaController extends Controller
{
     public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index($viagem_id)
    {
    	$dados['cronogramas'] = Cronograma::where('viagem_id', $viagem_id)->get();
    	$dados['viagem_id']   = $viagem_id; 
    	return view('dashboard.cronograma.listar', $dados);
    }

    public function novo($viagem_id)
    {
        $dados['viagem_id'] = $viagem_id;
    	return view('dashboard.cronograma.novo', $dados);
    }

    public function cadastrar(Request $request)
    {
    	
    	try {

    		DB::beginTransaction();
    		
    		$cronograma            = new Cronograma;
            $cronograma->titulo    = $request->input('titulo');
    		$cronograma->descricao = $request->input('descricao');
    		$cronograma->viagem_id = $request->input('viagem_id');
    		$cronograma->save();
    		
    		DB::commit();

    		return redirect('/admin/cronograma/' . $request->input('viagem_id'))->with('msg-ok', 'Cronograma Cadastrado com sucesso.');

    	} catch(\Exception $e) {
    		DB::rollback();
    		Log::error('CronogramaController@cadastrar: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
    	}
    }

    public function editar($id)
    {
    	try {
    		$dados['cronograma'] = Cronograma::findOrFail($id);
    		return view('dashboard.cronograma.editar', $dados);
    	} catch(\Exception $e) {
    		Log::error('CronogramaController@editar: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Cronograma não encontrada com o ID #' . $id);
    	}
    }

    public function atualizar(Request $request)
    {

        try {

            DB::beginTransaction();
            
            $cronograma            = Cronograma::findOrFail($request->input('id'));
            $cronograma->titulo    = $request->input('titulo');
            $cronograma->descricao = $request->input('descricao');
            $cronograma->save();
            
            DB::commit();

            return redirect('/admin/cronograma/editar/' . $request->input('id'))->with('msg-ok', 'Cronograma Atualizado com sucesso.');

        } catch(\Exception $e) {
            DB::rollback();
            Log::error('CronogramaController@atualizar: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
        }
    }

    public function remover($id)
    {
    	try {
    		$cronograma = Cronograma::findOrFail($id);
    	} catch(\Exception $e) {
    		Log::error('CronogramaController@remover: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Cronograma não encontrada com o ID #' . $id);
    	}

        try {
            $cronograma->delete();
            return redirect('/admin/cronograma/' . $cronograma->viagem_id)->with('msg-ok', 'Cronograma removida com sucesso.');
        } catch(\Exception $e) {
            Log::error('CronogramaController@remover: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Essa Cronograma possui alguns vinculos dentro do sistema, não pode ser removida.');
        }
            

    }
}
