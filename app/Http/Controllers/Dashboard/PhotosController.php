<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Log;
use App\Model\Photo;

class PhotosController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	$dados['photos'] = Photo::orderBy('id', 'desc')->paginate(10);
    	return view('dashboard.photo.listar', $dados);
    }

    public function novo()
    {
    	return view('dashboard.photo.add-imagem');
    }
    
    public function cadastrar(Request $request)
    {
        $photo = new Photo;

        if (!$request->hasFile('image')) {
            return redirect()->back()->with('msg-warning', 'Nenhuma imagem selecionada.');
        }

        try {
            $imagem = upload($request->file('image'), 'upload/gallery');
            $photo->url = $imagem;
            $photo->save();
            
            return redirect()->route('photo')->with('msg-ok', 'Imagem Cadastrada com sucesso.');
        } catch(\Exception $e) {    
            \Log::error('PhotoController@cadastrar: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro ao cadastrar a imagem.');
        }
        
    }

    public function remover($id)
    {
    	try {
    		$photo = Photo::findOrFail($id);
    	} catch(\Exception $e) {
    		Log::error('PhotoController@remover: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Foto não encontrado com o ID #' . $id);
    	}

        try {
            $photo->delete();
            return redirect()->route('photo')->with('msg-ok', 'Foto removido com sucesso.');
        } catch(\Exception $e) {
            Log::error('PhotoController@remover: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro ao remover a foto, tente novamente.');
        }

    }
}
