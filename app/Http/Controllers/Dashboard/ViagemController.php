<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DB;
use Log;
use Cache;
use App\Model\Viagem;

class ViagemController extends Controller
{

    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	$dados['viagens'] = Viagem::all();
    	return view('dashboard.viagem.listar', $dados);	
    }

    public function novo()
    {
    	return view('dashboard.viagem.novo');
    }

    public function cadastrar(Request $request)
    {
    	try {

    		DB::beginTransaction();
    		
    		$viagem                = new Viagem;
            $viagem->titulo        = $request->input('titulo');
    		$viagem->slug          = str_slug($request->input('titulo'));
    		$viagem->apresentacao  = $request->input('apresentacao');
    		$viagem->descricao     = $request->input('descricao');
    		$viagem->incluso       = $request->input('incluso');
    		$viagem->levar         = $request->input('levar');
    		$viagem->importante    = $request->input('importante');
            $viagem->valor         = $request->input('valor');
            $viagem->status        = $request->input('status');
            $viagem->saida_retorno = $request->input('saida_retorno');
            $viagem->observacao    = $request->input('observacao');
    		$viagem->save();
    		
    		DB::commit();

            Cache::forget('viagens');
    		
            return redirect('/admin/viagem')->with('msg-ok', 'Viagem Cadastrada com sucesso.');

    	} catch(\Exception $e) {
    		DB::rollback();
    		Log::error('ViagemController@cadastrar: ' . $e->getMessage());
    		return redirect()->back()->withInput()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
    	}
    }

    public function editar($id)
    {
    	try {
    		$dados['viagem'] = Viagem::findOrFail($id);
    		return view('dashboard.viagem.editar', $dados);
    	} catch(\Exception $e) {
    		Log::error('ViagemController@editar: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Viagem não encontrada com o ID #' . $id);
    	}
    }

    public function atualizar(Request $request)
    {
        try {

            DB::beginTransaction();
            
            $viagem                = Viagem::findOrFail($request->input('id'));
            $viagem->titulo        = $request->input('titulo');
            $viagem->slug          = str_slug($request->input('titulo'));
            $viagem->apresentacao  = $request->input('apresentacao');
            $viagem->descricao     = $request->input('descricao');
            $viagem->incluso       = $request->input('incluso');
            $viagem->levar         = $request->input('levar');
            $viagem->importante    = $request->input('importante');
            $viagem->valor         = $request->input('valor');
            $viagem->status        = $request->input('status');
            $viagem->saida_retorno = $request->input('saida_retorno');
            $viagem->observacao    = $request->input('observacao');
            $viagem->save();
            
            DB::commit();

            Cache::forget('viagens');

            return redirect('/admin/viagem/editar/' . $request->input('id'))->with('msg-ok', 'Viagem Atualizada com sucesso.');

        } catch(\Exception $e) {
            DB::rollback();
            Log::error('ViagemController@atualizar: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
        }
    }

    public function remover($id)
    {
    	try {
    		$viagem = Viagem::findOrFail($id);
    	} catch(\Exception $e) {
    		Log::error('ViagemController@remover: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Viagem não encontrada com o ID #' . $id);
    	}

        try {
            $viagem->delete();
            Cache::forget('viagens');
            return redirect('/admin/viagem')->with('msg-ok', 'Viagem removida com sucesso.');
        } catch(\Exception $e) {
            Log::error('ViagemController@remover: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Essa Viagem possui Vinculos com Estadis e/ou Cronogramas. Ela não pode ser removida.');
        }

    }

    public function addImagem($viagem_id)
    {
        $dados['viagem'] = Viagem::find($viagem_id);
        return view('dashboard.viagem.add-imagem', $dados);
    }

    public function atualizarImagem(Request $request)
    {
        $viagem = Viagem::find($request->input('viagem_id'));

        if (!$request->hasFile('imagem')) {
            return redirect()->back()->with('msg-warning', 'Nenhuma imagem selecionada.');
        }

        $path             = "upload/";
        $originalName     = $request->file('imagem')->getClientOriginalName();
        $originalNameCryp = md5($originalName . date('his'));
        $extension        = $request->file('imagem')->getClientOriginalExtension(); 
        $imagem           = sprintf("%s.%s", $originalNameCryp, $extension);
        $request->file('imagem')->move($path, $imagem);

        try {
            $viagem->imagem = $imagem;
            $viagem->save();
            Cache::forget('viagens');
            return redirect()->back()->with('msg-ok', 'Imagem Cadastrada com sucesso.');
        } catch(\Exception $e) {    
            \Log::error('ViagemController@atualizarImagem: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro ao cadastrar a imagem.');
        }
        
    }
}
