<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session;
use DB;
use Log;
use App\Model\Post;

class PostController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	$dados['posts'] = Post::orderBy('id', 'desc')->paginate(10);
    	return view('dashboard.post.listar', $dados);
    }

    public function novo()
    {
    	return view('dashboard.post.novo');
    }

    public function cadastrar(Request $request)
    {
        
    	try {

    		DB::beginTransaction();
    		
            $post = new Post;
            $post->title = $request->input('title');
            $post->slug = str_slug($request->input('title'));
            $post->content = $request->input('content');
            
            if ($request->hasFile('image')) {
                $post->image = upload($request->file('image'), 'upload/post');
            }

    		$post->save();
    		
    		DB::commit();

            return redirect()->route('post')->with('msg-ok', 'Post Cadastrado com sucesso.');

    	} catch(\Exception $e) {
    		DB::rollback();
    		Log::error('PostController@cadastrar: ' . $e->getMessage());
    		return redirect()->back()->withInput()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
    	}
    }

    public function editar($id)
    {
    	try {
    		$dados['post'] = Post::findOrFail($id);
    		return view('dashboard.post.editar', $dados);
    	} catch(\Exception $e) {
    		Log::error('PostController@editar: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Post não encontrado com o ID #' . $id);
    	}
    }

    public function atualizar(Request $request, $id)
    {
        
        try {

            DB::beginTransaction();
            
            $post = Post::findOrFail($id);
            $post->title = $request->input('title');
            $post->slug = str_slug($request->input('title'));
            $post->content = $request->input('content');

            if ($request->hasFile('image')) {
                $post->image = upload($request->file('image'), 'upload/post');
            }

            $post->save();
            DB::commit();

            return redirect()->back()->with('msg-ok', 'Post Atualizado com sucesso.');

        } catch(\Exception $e) {
            DB::rollback();
            Log::error('PostController@atualizar: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
        }
    }

    public function remover($id)
    {
    	try {
    		$post = Post::findOrFail($id);
    	} catch(\Exception $e) {
    		Log::error('PostController@remover: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Post não encontrado com o ID #' . $id);
    	}

        try {
            $post->delete();
            return redirect()->back()->with('msg-ok', 'Post removido com sucesso.');
        } catch(\Exception $e) {
            Log::error('PostController@remover: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro ao remover o post, tente novamente.');
        }

    }

}
