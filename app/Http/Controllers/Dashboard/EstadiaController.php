<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use DB;
use Log;
use App\Model\Estadia;

class EstadiaController extends Controller
{
     public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index($viagem_id)
    {
    	$dados['estadias']  = Estadia::where('viagem_id', $viagem_id)->get();
    	$dados['viagem_id'] = $viagem_id; 
    	return view('dashboard.estadia.listar', $dados);
    }

    public function novo($viagem_id)
    {
        $dados['viagem_id'] = $viagem_id;
    	return view('dashboard.estadia.novo', $dados);
    }

    public function cadastrar(Request $request)
    {
    	
    	try {

    		DB::beginTransaction();
    		
    		$estadia            = new Estadia;
    		$estadia->tipo      = $request->input('tipo');
    		$estadia->valor     = $request->input('valor');
    		$estadia->status    = $request->input('status');
    		$estadia->pagseguro = $request->input('pagseguro');
    		$estadia->viagem_id = $request->input('viagem_id');
    		$estadia->save();
    		
    		DB::commit();

    		return redirect('/admin/estadia/' . $request->input('viagem_id'))->with('msg-ok', 'Estadia Cadastrada com sucesso.');

    	} catch(\Exception $e) {
    		DB::rollback();
    		Log::error('EstadiaController@cadastrar: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
    	}
    }

    public function editar($id)
    {
    	try {
    		$dados['estadia'] = Estadia::findOrFail($id);
    		return view('dashboard.estadia.editar', $dados);
    	} catch(\Exception $e) {
    		Log::error('EstadiaController@editar: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Estadia não encontrada com o ID #' . $id);
    	}
    }

    public function atualizar(Request $request)
    {

        try {

            DB::beginTransaction();
            
            $estadia            = Estadia::findOrFail($request->input('id'));
            $estadia->tipo      = $request->input('tipo');
            $estadia->valor     = $request->input('valor');
            $estadia->status    = $request->input('status');
            $estadia->pagseguro = $request->input('pagseguro');
            $estadia->save();
            
            DB::commit();

            return redirect('/admin/estadia/editar/' . $request->input('id'))->with('msg-ok', 'Estadia Atualizada com sucesso.');

        } catch(\Exception $e) {
            DB::rollback();
            Log::error('EstadiaController@atualizar: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Ocorreu um erro no servidor, tente novamente.');
        }
    }

    public function remover($id)
    {
    	try {
    		$estadia = Estadia::findOrFail($id);
    	} catch(\Exception $e) {
    		Log::error('EstadiaController@remover: ' . $e->getMessage());
    		return redirect()->back()->with('msg-error', 'Estadia não encontrada com o ID #' . $id);
    	}

        try {
            $estadia->delete();
            return redirect('/admin/estadia/' . $estadia->viagem_id)->with('msg-ok', 'Estadia removida com sucesso.');
        } catch(\Exception $e) {
            Log::error('EstadiaController@remover: ' . $e->getMessage());
            return redirect()->back()->with('msg-error', 'Essa Estadia possui alguns vinculos dentro do sistema, não pode ser removida.');
        }
            

    }
}
