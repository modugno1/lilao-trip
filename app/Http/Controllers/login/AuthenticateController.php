<?php

namespace App\Http\Controllers\login;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;

class AuthenticateController extends Controller
{
    
    /**
     * Tela de Login
     * @return [type] [description]
     */
    public function login()
    {
    	if (Auth::guest()) {
    		return view('auth.login');
    	}
    	return redirect('/admin/dashboard');
    }

    /**
     * Autentica o usuário
     * @return [type] [description]
     */
    public function auth(Request $request)
    {
        // recupera os dados do formulário
        $dados = [];
        parse_str($request->input('data'), $dados);

        // valida se o e-mail existe
        if ( !$this->validEmail($dados['email']) ) {
            return response()->json(['status' => false, 'msg' => 'Esse e-mail não esta cadastrado.']);
        }

        $credentials = [
            'email' => $dados['email'],
            'password' => $dados['password']
        ];

        // valida as credenciais
    	if (!Auth::attempt($credentials)) {
            return response()->json(['status' => false, 'msg' => 'Senha Inválida.']);
        }
        
        // se tudo der certo, retorna com sucesso
        return response()->json(['status' => true, 'msg' => 'Autenticado, você será redirecionado.']);
        
    }

    /**
     * Desloga o usuário
     * @return [type] [description]
     */
    public function logout()
    {
    	if (Auth::check()) {
    		Auth::logout();
        }

    	return redirect('/login');
    }

    /**
     * Valida se o e-mail existe na base
     * @return bool
     */
    private function validEmail($email)
    {
        $user = User::where('email', $email)->first();
        return is_object($user);
    }

    /**
     * Verifica se o e-mail existe e retorna um json 
     * @param  Request $request [description]
     * @return json
     */
    public function checkEmail(Request $request)
    {
        // unserialize
        $dados = [];
        parse_str($request->input('data'), $dados);
        if ( !$this->validEmail($dados['email']) ) {
            return response()->json(['status' => false, 'msg' => 'Esse e-mail não esta cadastrado.']);
        }
        return response()->json(['status' => true]);
    }
}
