<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Viagem;

use App\Model\About;
use App\Model\Depoimento;
use App\Model\Banner;
use App\Model\Photo;

use Cache;
use Session;
use Mail;

class SiteController extends Controller
{

	private $cache_time;

    public function __construct()
    {
        // checa o ambiente que se encontra
        $this->middleware('producao');
    	$this->cache_time = 24*60;
    }
    public function home()
    {
    	$dados['viagens'] = Cache::remember('viagens', $this->cache_time, function() {
    		return Viagem::all();
    	});

    	$dados['title'] = "Home";
    	$dados['menu']  = "scroll";

        $dados['about'] = About::first();
        $dados['banners'] = Banner::orderBy('id', 'desc')->get();
        $dados['photos'] = Photo::orderBy('id', 'desc')->get();
        $dados['depoimentos'] = Depoimento::orderBy('created_at', 'desc')->get();

    	return view('site.home', $dados);
    }

    public function viagem($slug)
    {
    	$viagem = Viagem::where('slug', $slug)->first();

    	// se não for encontrado, vai para 404
    	if (!is_object($viagem)) {
    		abort(404);
    	}

    	$dados['viagem'] = $viagem;
    	$dados['title']  = $viagem->titulo;
    	$dados['menu']   = "normal";
        $dados['banners'] = Banner::orderBy('id', 'desc')->get();
        
    	return view('site.viagem', $dados);
    }

    public function enviarContato(Request $request)
    {
        $assunto = 'Lilão Trip - Site';
        if ($request->input('tipo') == 'news') {
            $assunto = 'Lilão Trip - NewsLetter';
        } elseif($request->input('tipo') == 'contato') {
            $assunto = 'Lilão Trip - Contato';
        }

        // email remetente
        $email = "contato@lilaotrip.com.br";

        try {
            Mail::send('emails.contato', $request->all(), function($msg) use ($email, $assunto) {
                $msg->subject($assunto);
                $msg->to($email);
            });
            
            return redirect('/enviado/contato')
                     ->with('msg-ok', 'Seu contato foi enviado com sucesso, em breve entraremos em contato.');

        } catch(\Exception $e) {
            \Log::error('SiteController@enviarContato: ' . $e->getMessage());
            return $e->getMessage();
            return redirect('enviado/contato')
                     ->with('msg-error', 'Ocorreu um erro ao enviar e-mail =[ Tente novamente: ' . $e->getMessage());
        }

    }
    
    public function contatoEnviado()
    {
        $dados['title']  = 'Contato Enviado';
        $dados['menu']   = "normal";
        return view('site.contato-enviado', $dados);
    }
    
}
