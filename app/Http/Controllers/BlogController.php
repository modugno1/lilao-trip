<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cache;
use Session;
use App\Model\Post;
use App\Model\Banner;

class BlogController extends Controller
{

    public function blog(Request $request)
    {
        
        $dados['search']  = $request->query('s');
    	$dados['title']   = 'Blog';
        $dados['menu']    = "normal";
        $dados['views']   = Post::orderBy('views', 'desc')->take(5)->get();
        $dados['recents'] = Post::orderBy('created_at', 'desc')->take(5)->get();
        $dados['banners'] = Banner::orderBy('id', 'desc')->get();
        
    	return view('site.blog', $dados);
    }
    
    public function post($slug)
    {
    	$post = Post::where('slug', $slug)->first();

    	// se não for encontrado, vai para 404
    	if (!is_object($post)) {
    		abort(404);
    	}

    	$dados['post'] = $post;
    	$dados['title']  = $post->title;
        $dados['menu']   = "normal";
        $dados['views'] = Post::orderBy('views', 'desc')->take(5)->get();
        $dados['recents'] = Post::orderBy('created_at', 'desc')->take(5)->get();
        
        $dados['banners'] = Banner::orderBy('id', 'desc')->get();
        
    	return view('site.post', $dados);
    }

    public function getAll(Request $request) {

        // variables
        $search = $request->query('s');
        $page   = ($request->query('page')) ? $request->query('page') : 1;
        $limit  = ($request->query('perPage')) ? $request->query('perPage') : 10;
        $schema = Post::orderBy('updated_at', 'desc');

        if ($search) {
            $schema->where("title", "LIKE", "%{$search}%");
        }

        if ($page) {
            $offset = ($page * $limit) - $limit;
            $schema->skip($offset)->take($limit);
        }

        $posts = $schema->get();
        $result = [];
        foreach ($posts as $post) {
            $post['createdAt'] = timestamp('2018-02-28 23:56:56', 'EUA')->format('d/m/Y H:i');
            $post['updatedAt'] = timestamp('2018-02-28 23:56:56', 'EUA')->format('d/m/Y H:i');
            $post['excerpt']   = str_limit($post->content, 150);
            array_push($result, $post);
        }

        return $result;
    }
}
