<?php

use Illuminate\Database\Seeder;
use App\Model\Post;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $title = $faker->sentence(6);
            Post::create([
                'title' => $title,
                'slug' => str_slug($title),
                'content' => $faker->text,
                'views' => $faker->numberBetween(0, 9999),
                'image' => 'https://fakeimg.pl/1000x300?text=Sem Imagem'
            ]);
        }
    }
}
