create table status (id int
					,nome_status varchar(20)
                    ,constraint pk_status_id primary key(id));

insert into status
	 values (0,'Inativo')
		   ,(1,'Ativo');

create table classificfragments (id int auto_increment
								,nome_classific varchar(30)
                                ,status_id int default 1
                                ,constraint pk_classificacao_id primary key(id)
                                ,constraint fk_classificacao_status_id foreign key (status_id) references status (id));

insert into classificfragments
	 values (1,'Single Page', 1)
		   ,(2,'CRM', 0)
           ,(3,'E-commerce', 0);
                           
create table tipofragments (id int auto_increment
						   ,nome_tipo varchar(30)
                           ,descricao_tipo varchar(200)
                           ,status_id int default 1
                           ,constraint pk_tipofragments_id primary key(id)
                           ,constraint fk_tipofragments_status_id foreign key (status_id) references status (id));
                           
insert into tipofragments
	 values (1,'header','Cabeçalho da página',1)
           ,(2,'content','Conteúdo principal da página',1)
           ,(3,'footer','Rodapé da página',1)
           ,(4,'nav','Menu principal de navegação da página',1)
           ,(5,'sidebar','Coluna lateral da página',1)
           ,(6,'popup','Submenu dos elementos da página',1);

create table templatefragments (id int auto_increment
							   ,users_id int not null
                               ,classificfragments_id int not null
                               ,tipofragments_id int not null
                               ,thumbnail_path varchar(150)
                               ,html varchar(20000)
                               ,javascript varchar(20000)
                               ,css varchar(20000)
                               ,dtcadastro datetime
							   ,status_id int default 1
							   ,constraint pk_templatefragments_id primary key(id)
                               ,constraint fk_templatefragments_users_id foreign key (users_id) references users (id)
                               ,constraint fk_templatefragments_classificfragments_id foreign key (classificfragments_id) references classificfragments (id)
							   ,constraint fk_templatefragments_tipofragments_id foreign key (tipofragments_id) references tipofragments (id)
							   ,constraint fk_templatefragments_status_id foreign key (status_id) references status (id));
                           
create table configtemplatefragments (id int auto_increment
									 ,templatefragments_id int not null
									 ,campo varchar(50)
									 ,valor varchar(3000)
                                     ,status_id int default 1
                                     ,constraint pk_configtemplatefragments_id primary key(id)
                                     ,constraint fk_configtemplatefragments_templatefragments_id foreign key (templatefragments_id) references templatefragments (id)
                                     ,constraint fk_configtemplatefragments_status_id foreign key (status_id) references status (id));
                                     
create table fragments (id int auto_increment
					   ,users_id int not null
                       ,classificfragments_id int not null
                       ,tipofragments_id int not null
                       ,thumbnail_path varchar(150)
                       ,html varchar(20000)
                       ,javascript varchar(20000)
                       ,css varchar(20000)
                       ,dtcadastro datetime
					   ,status_id int default 1
					   ,constraint pk_fragments_id primary key(id)
                       ,constraint fk_fragments_users_id foreign key (users_id) references users (id)
                       ,constraint fk_fragments_classificfragments_id foreign key (classificfragments_id) references classificfragments (id)
					   ,constraint fk_fragments_tipofragments_id foreign key (tipofragments_id) references tipofragments (id)
					   ,constraint fk_fragments_status_id foreign key (status_id) references status (id));
                           
create table configfragments (id int auto_increment
							 ,fragments_id int not null
							 ,campo varchar(50)
							 ,valor varchar(3000)
                             ,status_id int default 1
                             ,constraint pk_configfragments_id primary key(id)
                             ,constraint fk_configfragments_fragments_id foreign key (fragments_id) references fragments (id)
                             ,constraint fk_configfragments_status_id foreign key (status_id) references status (id));