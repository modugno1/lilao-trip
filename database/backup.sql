/*

SQLyog Ultimate v8.55 
MySQL - 5.6.34-log : Database - fragments

*********************************************************************

*/



/*!40101 SET NAMES utf8 */;



/*!40101 SET SQL_MODE=''*/;



/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`fragments` /*!40100 DEFAULT CHARACTER SET latin1 */;



USE `fragments`;



/*Table structure for table `classificfragments` */



DROP TABLE IF EXISTS `classificfragments`;



CREATE TABLE `classificfragments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_classific` varchar(30) DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_classificacao_status_id` (`status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



/*Data for the table `classificfragments` */



insert  into `classificfragments`(`id`,`nome_classific`,`status_id`) values (1,'Single Page',1),(2,'CRM',0),(3,'E-commerce',0);



/*Table structure for table `configfragments` */



DROP TABLE IF EXISTS `configfragments`;



CREATE TABLE `configfragments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fragments_id` int(11) NOT NULL,
  `campo` varchar(50) DEFAULT NULL,
  `valor` varchar(3000) DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_configfragments_fragments_id` (`fragments_id`),
  KEY `fk_configfragments_status_id` (`status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



/*Data for the table `configfragments` */



/*Table structure for table `configtemplatefragments` */



DROP TABLE IF EXISTS `configtemplatefragments`;



CREATE TABLE `configtemplatefragments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templatefragments_id` int(11) NOT NULL,
  `campo` varchar(50) DEFAULT NULL,
  `valor` varchar(3000) DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_configtemplatefragments_templatefragments_id` (`templatefragments_id`),
  KEY `fk_configtemplatefragments_status_id` (`status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



/*Data for the table `configtemplatefragments` */



/*Table structure for table `fragments` */



DROP TABLE IF EXISTS `fragments`;



CREATE TABLE `fragments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `classificfragments_id` int(11) NOT NULL,
  `tipofragments_id` int(11) NOT NULL,
  `thumbnail_path` varchar(150) DEFAULT NULL,
  `html` varchar(20000) DEFAULT NULL,
  `javascript` varchar(20000) DEFAULT NULL,
  `css` varchar(20000) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_fragments_users_id` (`users_id`),
  KEY `fk_fragments_classificfragments_id` (`classificfragments_id`),
  KEY `fk_fragments_tipofragments_id` (`tipofragments_id`),
  KEY `fk_fragments_status_id` (`status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



/*Data for the table `fragments` */



/*Table structure for table `migrations` */



DROP TABLE IF EXISTS `migrations`;



CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



/*Data for the table `migrations` */



insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);



/*Table structure for table `password_resets` */



DROP TABLE IF EXISTS `password_resets`;



CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



/*Data for the table `password_resets` */



/*Table structure for table `status` */



DROP TABLE IF EXISTS `status`;



CREATE TABLE `status` (
  `id` int(11) NOT NULL DEFAULT '0',
  `nome_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



/*Data for the table `status` */



insert  into `status`(`id`,`nome_status`) values (0,'Inativo'),(1,'Ativo');



/*Table structure for table `templatefragments` */



DROP TABLE IF EXISTS `templatefragments`;



CREATE TABLE `templatefragments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `classificfragments_id` int(11) NOT NULL,
  `tipofragments_id` int(11) NOT NULL,
  `thumbnail_path` varchar(150) DEFAULT NULL,
  `html` varchar(20000) DEFAULT NULL,
  `javascript` varchar(20000) DEFAULT NULL,
  `css` varchar(20000) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_templatefragments_users_id` (`users_id`),
  KEY `fk_templatefragments_classificfragments_id` (`classificfragments_id`),
  KEY `fk_templatefragments_tipofragments_id` (`tipofragments_id`),
  KEY `fk_templatefragments_status_id` (`status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



/*Data for the table `templatefragments` */



/*Table structure for table `tipofragments` */



DROP TABLE IF EXISTS `tipofragments`;



CREATE TABLE `tipofragments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_tipo` varchar(30) DEFAULT NULL,
  `descricao_tipo` varchar(200) DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_tipofragments_status_id` (`status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;



/*Data for the table `tipofragments` */



insert  into `tipofragments`(`id`,`nome_tipo`,`descricao_tipo`,`status_id`) values (1,'header','Cabeçalho da página',1),(2,'content','Conteúdo principal da página',1),(3,'footer','Rodapé da página',1),(4,'nav','Menu principal de navegação da página',1),(5,'sidebar','Coluna lateral da página',1),(6,'popup','Submenu dos elementos da página',1);



/*Table structure for table `users` */



DROP TABLE IF EXISTS `users`;



CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



/*Data for the table `users` */



insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values (1,'Guilherme Modugno','g.modugno@hotmail.com','$2y$10$EzFsDb5f45RVki48NEGbw.94xOq2B15QfVriR9zSC.5MRwRpGBfui','wHsIKLiAbA6FUpljhj0AXO7TTB6haZoGkFjAREnwWHwJDniagMCZkXqUZN7s',NULL,'2016-12-27 15:47:21'),(2,'Jonathan Araujo','jonathan@hotmail.com','$2y$10$EzFsDb5f45RVki48NEGbw.94xOq2B15QfVriR9zSC.5MRwRpGBfui',NULL,NULL,NULL);



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;

/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

