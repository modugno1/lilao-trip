@extends('site.site')
@section('title', $title)
@section('content')
<!-- quem somos -->
<div class="about" id="quemsomos">
<div class="w3-about-top">
<!-- <h3>Q</h3> -->
</div>
<div class="w3l-about">
<div class="container">
<div class="col-md-12">
<div class="w3ls-heading">
<h2>{{$about->title_about}}</h2>
</div>
<div class="w3ls-about-info sobre-texto">
<p>
{{$about->content_about}}
</p>
</div>
</div>
<div class="col-md-12 porque-escolher">
<div class="w3ls-heading">
@if ($about->title_why)
<h2>{{ $about->title_why }}</h2>
@endif
</div>
<div class="w3ls-about-info">
@if ($about->content_why)
<p>{{$about->content_why}}</p>
@endif
</div>
</div>

<div class="col-md-12">
<hr>
</div>

@php
$cols = (!empty($about->video_one) && !empty($about->video_two)) ? 'col-md-6' : 'col-md-12'
@endphp
@if ($about->video_one)
<div class="{{$cols}}">
<iframe class="sobre-video" width="560" height="230" src="https://www.youtube.com/embed/{{$about->video_one}}" frameborder="0" allowfullscreen></iframe>
	</div>
	@endif
	@if (!empty($about->video_two))
	<div class="{{$cols}}">
	<iframe class="sobre-video" width="560" height="230" src="https://www.youtube.com/embed/{{$about->video_two}}" frameborder="0" allowfullscreen></iframe>
		</div>
		@endif
		</div>
		</div>
		</div>
		<!-- //about -->
		<!-- services -->
		<div class="services" id="viagens">
		<div class="w3-services-top">
		<!-- <h3>V</h3> -->
		</div>
		<div class="w3l-about w3l-services">
		<div class="container">
		<div class="w3ls-heading">
		<h3>Pacotes de Viagens</h3>
		</div>
		<div class="agileits-services-grids">
		
		@forelse($viagens as $viagem)
		<!-- card viagem -->
		<article class="col-xs-12 col-sm-6 col-md-4"><!-- agileits-services-grid -->
		<figure class="viagens">
		@if (!empty($viagem->imagem))
		<img src="/upload/{{ $viagem->imagem }}" alt="{{ $viagem->titulo }}">
		@else
		<img src="http://placehold.it/250x150">
		@endif
		<figcaption class="viagem-caption">
		<div class="col-xs-12">
		<h4 class="viagem-titulo pull-left">{{ $viagem->titulo }}</h4>
		@if ($viagem->status == 1)
		<span class="label label-info pull-right viagem-status">Disponível</span>
		@else
		<span class="label label-danger pull-right viagem-status">Indisponível</span>
		@endif
		</div>
		<div class="col-xs-12">
		<hr>
		</div>
		
		<div class="col-xs-12">
		<span class="viagem-preco-apartir pull-left">à partir de</span>
		<span class="viagem-preco pull-left">{{ $viagem->valor }}</span>	
		<a href="{{ url('viagem/' . $viagem->slug) }}#viagem" class="btn btn-success pull-right">Detalhes</a>
		</div>
		
		<div class="col-xs-12">
			<span class="viagem-observacao">{{ $viagem->observacao }}</span>
		</div>
		
		</figcaption>
		</figure>	
		</article>
		<!-- fim card viagem -->
		@empty
		<h4 class="text-center text-muted">Nenhuma viagem ainda =[</h4>
		@endforelse
		
		<div class="clearfix"> </div>
		</div>
		</div>
		</div>
		</div>
		<!-- //services -->
		<!-- gallery -->
		<div class="gallery" id="fotos">
		<div class="w3-gallery-top">
		<!-- <h3>G</h3> -->
		</div>
		<div class="w3l-about w3l-gallery">
		<div class="container">
		<div class="w3ls-heading">
		<h3>Fotos</h3>
		</div>
		<div class="gallery-grids">
		@forelse ($photos as $photo)
		<div class="col-md-3 gallery-grid">
		<div class="grid">
		<figure class="effect-apollo">
		<a class="example-image-link" href="{{ asset('upload/gallery/' . $photo->url) }}" data-lightbox="example-set" data-title="">
		<img src="{{ asset('upload/gallery/' . $photo->url) }}" alt="" />
		<figcaption>
		</figcaption>	
		</a>
		</figure>
		</div>
		</div>
		@empty
		<h1 class="text-center">Nenhuma foto publicada.</h1>
		@endforelse
		<div class="clearfix"> </div>
		<script src="/site/js/lightbox-plus-jquery.min.js"> </script>
		</div>
		</div>
		</div>
		</div>
		<!-- //gallery -->
		<!-- testimonial -->
		<div class="testimonial" id="depoimentos">
		<div class="w3-testimonial-top">
		<!-- <h3>T</h3> -->
		</div>
		<div class="w3l-about w3l-testimonial">
		<div class="container">
		<div class="w3ls-heading">
		<h3>Depoimentos</h3>
		</div>
		<div class="w3-agile-testimonial">
		<div class="slider">
		<div class="callbacks_container">
		<ul class="rslides callbacks callbacks1" id="slider3">
		@forelse ($depoimentos as $depo)
		<li>
		<div class="testimonial-img-info">
		<p><i class="fa fa-quote-left" aria-hidden="true"></i>
		{{ $depo->content }}
		<i class="fa fa-quote-right" aria-hidden="true"></i></p>
		<h5>{{ $depo->name }}</h5>
		<h6>{{ $depo->date }}</h6>
		</div>
		</li>
		@empty
		<h2 class="text-center">Nenhum depoimento enviado.</h2>
		@endforelse
		
		</ul>
		</div>
		<div class="clearfix"> </div>
		<script>
		// You can also use "$(window).load(function() {"
			$(function () {
				// Slideshow 4
				$("#slider3").responsiveSlides({
					auto: false,
					pager:true,
					nav:true,
					speed: 500,
					timeout: 5000,
					namespace: "callbacks",
					before: function () {
						$('.events').append("<li>before event fired.</li>");
					},
					after: function () {
						$('.events').append("<li>after event fired.</li>");
					}
				});
				
			});
			</script>
			<!--banner Slider starts Here-->
			</div>
			</div>
			</div>
			</div>
			</div>
			<!-- //testimonial -->
			
			<!-- subscribe -->
			<div class="subscribe">
			<div class="w3-subscribe-top">
			<!-- <h3>S</h3> -->
			</div>
			<div class="w3l-about w3l-team">
			<div class="container">
			<div class="w3ls-heading">
			<h3>Newsletter</h3>
			</div>
			<div class="w3-agile-subscribe">
			<p>Fique por dentro de promoções e pacotes de viagens!</p>
			<form action="{{ url('contato/enviar') }}" method="POST">
			{!!  csrf_field() !!}
			<input type="hidden" name="tipo" value="news">
			<input type="email" id="mc4wp_email" name="email" placeholder="Digite seu e-mail" required="">
			<input type="submit" value="Enviar">
			</form>
			<h5>Conecte-se com a gente</h5>
			<div class="agileinfo-social-grids">
			<ul>
			<li><a target="_blank" href="https://www.facebook.com/lilaotrip"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li><a target="_blank" href="http://instagram.com/lilaotrip"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			<li><a target="_blank" href="https://www.youtube.com/channel/UCDR-c3zWYDGbDPRd-lqR5gA"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
			</ul>
			</div>
			</div>
			</div>
			</div>
			</div>
			<!-- //subscribe -->
			<div class="col-md-12">
			<hr>
			</div>
			@endsection