<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="pt-BR">
 <head>

  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="2015.2.0.352"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  
  
  <title>Início</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="/construcao/css/site_global.css?crc=3916556066"/>
  <link rel="stylesheet" type="text/css" href="/construcao/css/index.css?crc=215765948" id="pagesheet"/>
  <!-- IE-only CSS -->
  <!--[if lt IE 9]>
  <link rel="stylesheet" type="text/css" href="/construcao/css/iefonts_index.css?crc=55390305"/>
  <![endif]-->
  <!-- Other scripts -->
  
  <!-- JS includes -->
   </head>
 <body>

  <div class="clearfix borderbox" id="page"><!-- column -->
   <div class="clip_frame colelem" id="u207"><!-- image -->
    <img class="block" id="u207_img" src="/construcao/images/13-3.png?crc=4173938890" alt="" width="143" height="174"/>
   </div>
   <div class="browser_width colelem" id="u97-4-bw">
    <div class="clearfix" id="u97-4"><!-- content -->
     <p>SITE EM CONTRUÇÃO</p>
    </div>
   </div>
   <div class="browser_width colelem" id="u100-bw">
    <div id="u100"><!-- simple frame --></div>
   </div>
   <div class="browser_width colelem" id="u98-4-bw">
    <div class="clearfix" id="u98-4"><!-- content -->
     <p>Entre em contato:</p>
    </div>
   </div>
   <form class="form-grp clearfix colelem" id="widgetu101" method="get" enctype="multipart/form-data" action="/manutencao">
    {!! csrf_field() !!}
    <input type="hidden" id="">
   <!-- none box -->
    <div class="fld-grp clearfix grpelem" id="widgetu111" data-required="true"><!-- none box -->
     <label class="fld-label actAsDiv clearfix grpelem" id="u114-4" for="widgetu111_input"><!-- content --><span class="actAsPara">Nome:</span></label>
     <span class="fld-input NoWrap actAsDiv clearfix grpelem" id="u113-4"><!-- content --><input class="wrapped-input" type="text" spellcheck="false" id="widgetu111_input" name="primeiro_nome" tabindex="1"/><label class="wrapped-input fld-prompt" id="widgetu111_prompt" for="widgetu111_input"><span class="actAsPara">Inserir nome</span></label></span>
    </div>
    <div class="fld-grp clearfix grpelem" id="widgetu102" data-required="true" data-type="email"><!-- none box -->
     <label class="fld-label actAsDiv clearfix grpelem" id="u105-4" for="widgetu102_input"><!-- content --><span class="actAsPara">Email:</span></label>
     <span class="fld-input NoWrap actAsDiv clearfix grpelem" id="u103-4"><!-- content --><input class="wrapped-input" type="email" spellcheck="false" id="widgetu102_input" name="email" tabindex="2"/><label class="wrapped-input fld-prompt" id="widgetu102_prompt" for="widgetu102_input"><span class="actAsPara">Inserir email</span></label></span>
    </div>
    <div class="clearfix grpelem" id="u106-4"><!-- content -->
     <p>Enviando formulário…</p>
    </div>
    <div class="clearfix grpelem" id="u116-4"><!-- content -->
     <p>O servidor encontrou um erro.</p>
    </div>
    <div class="clearfix grpelem" id="u117-4"><!-- content -->
     <p>Formulário recebido.</p>
    </div>
    <input class="submit-btn NoWrap grpelem" id="u115-17" type="submit" value="" tabindex="4"/><!-- state-based BG images -->
    <div class="fld-grp clearfix grpelem" id="widgetu107" data-required="false"><!-- none box -->
     <label class="fld-label actAsDiv clearfix grpelem" id="u108-4" for="widgetu107_input"><!-- content --><span class="actAsPara">Mensagem:</span></label>
     <span class="fld-textarea actAsDiv clearfix grpelem" id="u109-4"><!-- content --><textarea class="wrapped-input" id="widgetu107_input" name="mensagem" tabindex="3"></textarea><label class="wrapped-input fld-prompt" id="widgetu107_prompt" for="widgetu107_input"><span class="actAsPara">Insira sua mensagem</span></label></span>
    </div>
   </form>
   <div class="verticalspacer" data-offset-top="604" data-content-above-spacer="603" data-content-below-spacer="200"></div>
   <div class="clearfix colelem" id="pu94"><!-- group -->
    <div class="browser_width grpelem" id="u94-bw">
     <div id="u94"><!-- group -->
      <div class="clearfix" id="u94_align_to_page">
       <div class="clearfix grpelem" id="u118"><!-- group -->
        <a class="nonblock nontext clip_frame grpelem" id="u119" href="https://www.facebook.com/lilaotrip/?fref=ts"><!-- image --><img class="block" id="u119_img" src="/construcao/images/facebook-logo.png?crc=490547617" alt="" width="22" height="22"/></a>
        <a class="nonblock nontext clip_frame grpelem" id="u121" href="https://www.instagram.com/lilaotrip/"><!-- image --><img class="block" id="u121_img" src="/construcao/images/instagram-logo.png?crc=239101142" alt="" width="22" height="22"/></a>
       </div>
      </div>
     </div>
    </div>
    <div class="browser_width grpelem" id="u99-6-bw">
     <div class="clearfix" id="u99-6"><!-- content -->
      <p>(11) 2985-6681 - (11) 99504-2418<br/>contato@lilaotrip.com.br</p>
     </div>
    </div>
   </div>
  </div>
  <div class="preload_images">
   <img class="preload" src="/construcao/images/u115-17-r.png?crc=3935254141" alt=""/>
   <img class="preload" src="/construcao/images/u115-17-m.png?crc=78979498" alt=""/>
   <img class="preload" src="/construcao/images/u115-17-fs.png?crc=3939754534" alt=""/>
  </div>
  
   </body>
</html>
