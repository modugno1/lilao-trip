@extends('site.site')
@section('title', $title)
@section('content')
<div class="about" id="viagem">
		<a name='viagem'></a>
		<div class="w3-about-top">
			<!-- <h3>V</h3> -->
		</div>
		<div class="w3l-about">
			<div class="container">
				
				<!-- informações básicas viagem -->
				<section class="row viagem-single-basico">
					<div class="col-sm-3">
						@if (!empty($viagem->imagem))
							<img src="/upload/{{ $viagem->imagem }}" alt="{{ $viagem->titulo }}">
						@else
							<img src="http://placehold.it/250x150">
						@endif
					</div>
					<div class="col-sm-9">
						<h1 class="viagem-single-titulo">{{ $viagem->titulo }}</h1>
						<h3 class="text-muted">{{ $viagem->valor }}</h3>
						<h5 class="text-warning">{{ $viagem->observacao }}</h5>
						<p class="text-muted">
							{{ $viagem->apresentacao }}
						</p>
					</div>
				</section>
				<!-- fim informações básicas viagem -->
				
				<!-- informações gerais viagem -->
				<section class="row viagem-single-geral">
					<div class="col-md-12">
						<h2>Saídas Disponíveis</h2>
						<hr>
						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>Saída/Retorno</th>
										<th>Status</th>
										<th>A partir de</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<strong class="text-success">{{ $viagem->saida_retorno }}</strong>
										</td>
										<td>
											@if ($viagem->status == 1)
												<span class="label label-info">Disponível</span>
											@else
												<span class="label label-danger">Indisponível</span>
											@endif
										</td>
										<td>{{ $viagem->valor }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="col-md-12">
						 <!-- Nav tabs -->
						  <ul class="nav nav-tabs" role="tablist">
						    <li role="presentation">
						    	<a href="#descricao" aria-controls="descricao" role="tab" data-toggle="tab">Descrição</a>
						    </li>
						    <li role="presentation" class="active">
						    	<a href="#estadias" aria-controls="estadias" role="tab" data-toggle="tab">Estadias</a>
						    </li>
						    <li role="presentation">
						    	<a href="#cronograma" aria-controls="cronograma" role="tab" data-toggle="tab">Cronograma</a>
						    </li>
						    <li role="presentation">
						    	<a href="#oque-incluso" aria-controls="oque-incluso" role="tab" data-toggle="tab">O que está incluso</a>
						    </li>
						    <li role="presentation">
						    	<a href="#oque-levar" aria-controls="oque-levar" role="tab" data-toggle="tab">O que levar</a>
						    </li>
						    <li role="presentation">
						    	<a href="#info-importante" aria-controls="info-importante" role="tab" data-toggle="tab">Info. Importantes</a>
						    </li>
						  </ul>

						  <!-- Tab panes -->
						  <div class="tab-content">
						  	<!-- descrição -->
						    <div role="tabpanel" class="tab-pane" id="descricao">
						    	<div class="col-md-12">
						    		<h3>Descrição</h3>
						    		<hr>
						    	</div>
						    	<div class="col-md-12">
						    		<div class="text-muted">
						    			<?= $viagem->descricao ?>
						    		</div>
						    	</div>
						    </div>
						    <!-- fim descrição -->
							
							<!-- Estadias -->
						    <div role="tabpanel" class="tab-pane active" id="estadias">
						    	<div class="table-responsive">
						    		<table class="table table-hover table-striped">
						    			<thead>
						    				<tr class="info">
						    					<th>Tipo</th>
						    					<th>Valor</th>
						    					<th>Status</th>
						    					<th>Comprar Agora</th>
						    				</tr>
						    			</thead>
						    			<tbody>

						    			@forelse ($viagem->estadias as $estadia)
						    				<tr>
						    					<td>{{ $estadia->tipo }}</td>
						    					<td>{{ $estadia->valor }}</td>
						    					<td>
						    						@if ($estadia->status == 1)
														<span class="label label-info">Disponível</span>
													@else
														<span class="label label-danger">Indisponível</span>
													@endif
						    					</td>
						    					<td>
						    						<a href="{{ $estadia->pagseguro }}" target="_blank">Comprar</a>
						    					</td>
						    				</tr>
						    			@empty
						    				<h4 class="text-center">Nenhuma Estadia Cadatrada</h4>
						    			@endforelse
						    			</tbody>
						    		</table>
						    	</div>
						    </div>
						    <!-- fim Estadias -->

						    <!-- cronograma -->
						    <div role="tabpanel" class="tab-pane" id="cronograma">
						    	<div class="col-md-12">
						    		<h3>Cronograma</h3>
						    		<hr>
						    	</div>
						    	<!-- cronogramas -->
						    	<div class="col-md-12">
						    		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
										<?php $aux = 0; ?>
						    			@forelse ($viagem->cronogramas as $cronograma)
						    			<?php
						    				$expanded = 'false';
						    				$classe   = '';

						    				if ($aux == 0) {
						    					$expanded = 'true';
						    					$classe   = 'in';
						    					$aux      = 1;
						    				}

						    			?>
						    			{{-- inicio cronograma --}}
						    			<div class="panel panel-default">
						    				<div class="panel-heading" role="tab" id="headingOne">
						    					<h4 class="panel-title">
						    						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#dia-{{ $cronograma->id }}" aria-expanded="{{ $expanded }}" aria-controls="collapseOne">
						    						{{ $cronograma->titulo }}
						    						</a>
						    					</h4>
						    				</div>
						    				<div id="dia-{{ $cronograma->id }}" class="panel-collapse collapse {{ $classe }} " role="tabpanel" aria-labelledby="headingOne">
						    					<div class="panel-body">
						    						<?= $cronograma->descricao ?>
						    					</div>
						    				</div>
						    			</div>
						    			{{-- fim cronograma --}}
						    			@empty
						    				<h4 class="text-center">Nenhum cronograma Cadastrado</h4>
						    			@endforelse
						    		</div>
						    	</div>
						    	<!-- fim cronogramas -->
						    </div>
						    <!-- fim cronograma -->

						    <!-- o que esta incluso -->
						    <div role="tabpanel" class="tab-pane" id="oque-incluso">
						    	<div class="col-md-12">
						    		<h3>O que está incluso?</h3>
						    		<hr>
						    	</div>
						    	<div class="col-md-12">
						    		<div class="text-muted">
						    			<?= $viagem->incluso ?>
						    		</div>
						    	</div>
						    </div>
						    <!-- fim o que esta incluso -->

						    <!-- que levar -->
						    <div role="tabpanel" class="tab-pane" id="oque-levar">
						    	<div class="col-md-12">
						    		<h3>O que levar?</h3>
						    		<hr>
						    	</div>
						    	<div class="col-md-12">
						    		<div class="text-muted">
						    			<?= $viagem->levar ?>
						    		</div>
						    	</div>
						    </div>
						    <!-- fim o que levar -->

						    <!-- info importante -->
						    <div role="tabpanel" class="tab-pane" id="info-importante">
						    	<div class="col-md-12">
						    		<h3>Informações Importantes</h3>
						    		<hr>
						    	</div>
						    	<div class="col-md-12">
						    		<div class="text-muted">
						    			<?= $viagem->importante ?>
						    		</div>
						    	</div>
						    </div>
						    <!-- fim info importante -->
						  </div>
					</div>
				</section>
				<!-- fim informações gerais viagem -->

				<div class="col-md-12">
					<hr>
				</div>

			</div>
		</div>
	</div>
@endsection