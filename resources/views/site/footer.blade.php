	<!-- contact -->
	<div class="contact" id="contato">
		<div class="w3-contact-top">
			<!-- <h3>C</h3> -->
		</div>
		<div class="w3l-about w3l-team">
			<div class="container">
				<div class="w3ls-heading">
					<h3>Contato</h3>
				</div>
				<div class="agile-contact-grids">
					<div class="col-md-5 address">
						<h4>Informações</h4>
						<div class="address-row">
							<div class="col-md-2 w3-agile-address-left">
								<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
							</div>
							<div class="col-md-10 w3-agile-address-right">
								<h5>E-mail</h5>
								<p><a href="mailto:contato@lilaotrip.com.br"> contato@lilaotrip.com.br</a></p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="address-row">
							<div class="col-md-2 w3-agile-address-left">
								<span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
							</div>
							<div class="col-md-10 w3-agile-address-right">
								<h5>Central de Atendimento</h5>
								<p>(11) 2985-6681 - (11) 94045-2281 <br> Seg. à Sex. das 9h às 18h</p>
							</div>
							<div class="clearfix"> </div>
						</div>

						<div class="address-row">
							<div class="col-md-2 w3-agile-address-left">
								<span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
							</div>
							<div class="col-md-10 w3-agile-address-right">
								<h5>Formas de Pagamento</h5>
								<p><img src="/site/images/formas-pagamento.png" width="200px" alt="Pagseguro"></p>
							</div>
							<div class="clearfix"> </div>
						</div>

						<div class="address-row">
							<div class="col-md-2 w3-agile-address-left">
								<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
							</div>
							<div class="col-md-10 w3-agile-address-right">
								<h5>Curta nossa página</h5>
								<div class="fb-like" data-href="https://www.facebook.com/lilaotrip" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
							</div>
							<div class="clearfix"> </div>
						</div>

					</div>
					<div class="col-md-7 contact-form">
						<form action="{{ url('contato/enviar') }}" method="POST">
							{!!  csrf_field() !!}
							<input type="hidden" name="tipo" value="contato">
							<input type="text" name="primeiro_nome" placeholder="Primeiro Nome" required>
							<input class="email" name="segundo_nome" type="text" placeholder="Segundo Nome" required>
							<input type="text" name="tel" placeholder="Telefone ou Celular (opcional)">
							<input class="email" name="email" type="email" placeholder="Email" required>
							<textarea name="mensagem" placeholder="Mensagem" required></textarea>
							<input type="submit" value="Enviar">
						</form>
					</div>
					<div class="clearfix"> </div>	
				</div>
			</div>
		</div>
	</div>
	<!-- //contact -->
	<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="copyright">
				<p>© 2017 Lilão Trip . Todos os Direitos Reservados | Desenvolvido por <a href="https://transmitadigital.com.br/" title="Transmita Digital" alt="Transmita Digital" target="_blank">Transmita Digital</a> </p>
			</div>
		</div>
		<a href="#" class="to-top">
			<span class="glyphicon glyphicon-chevron-up"></span>
			Voltar ao Topo
		</a>
	</div>
	<!-- //footer -->