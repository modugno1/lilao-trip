@extends('site.site')
@section('title', $title)
@section('content')
<script>
	$(document).ready(function() {

		window.onload = function () {
			
			$('html, body').animate({
				scrollTop: $('#quemsomos').offset().top
			});
		}

	});
</script>
<!-- quem somos -->
	<div class="about" id="quemsomos">
		<section class="row">
			<div class="container">
				<div class="col-md-12">
					
					@if(Session::has('msg-ok'))
						<h2 class='alert alert-success'>{{ Session::get('msg-ok') }}</h2>
					@elseif(Session::has('msg-error'))
						<h2 class='alert alert-danger'>{{ Session::get('msg-error') }}</h2>
					@endif

					<hr>
				</div>
			</div>
		</section>
		
	</div>
@endsection