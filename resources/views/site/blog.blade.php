@extends('site.site')
@section('title', $title)
@section('content')
<div class="about" id="blog">
		<a name='blog'></a>
		<div class="w3-about-top">
			<!-- <h3>V</h3> -->
		</div>
		<div class="w3l-about">
			<div class="container">
                
                <div class="row">
                    <a name='posts'></a>
                    <!-- left -->
                    <input type="hidden" id="searchQuery" value="{{$search}}">
                    <section class="col-md-9" class="l-post">
                        <div class="loading">
                            <img src="{{ asset('site/images/loading.svg') }}" alt="Carregando...">
                        </div>
                        <div id="post-container"><!-- posts here --></div>

                        <div class="text-center">
                            <button type="button" id="loadmore" class="btn btn-lilao pull">Carregar mais</button>
                        </div>
                    </section> <!-- ./end-left -->

                    <!-- right -->
                    <aside class="col-md-3">
                        <form id="search" action="/blog">
                        <div class="input-group">
                            <input type="search" class="form-control" id="search_input" placeholder="Pesquisar">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-search"></span>
                            </span>
                        </div>
                        </form>

                        <div class="form-group maislidas">
                            <h3 class="text-muted">Mais Lidos</h3>
                            <ul class="posts-list">
                                @forelse ($views as $v)
                                <li>
                                    <a href="{{ route('blogsite.post', ['id' => $v->slug]) }}#posts">{{ $v->title }}</a>
                                </li>
                                @empty
                                    <li>Nenhum Post</li>
                                @endforelse
                            </ul>
                        </div>

                        <div class="form-group recents">
                            <h3 class="text-muted">Últimos Posts</h3>
                            <ul class="posts-list">
                                @forelse ($recents as $r)
                                <li>
                                    <a href="{{ route('blogsite.post', ['id' => $r->slug]) }}#posts">{{ $r->title }}</a>
                                </li>
                                @empty
                                    <li>Nenhum Post</li>
                                @endforelse
                            </ul>
                        </div>
                    </aside><!-- ./end-right -->
                </div>
            
				<div class="col-md-12">
					<hr>
				</div>

			</div>
		</div>
    </div>
    
<script src="{{ asset('site/js/blog.js') }}"></script>
@endsection