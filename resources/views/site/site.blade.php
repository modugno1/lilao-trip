<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<title>Lilão Trip - @yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta charset="utf-8">
<meta name="keywords" content="Global Tours Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script> -->
<!-- bootstrap-css -->
<link href="/site/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
<link rel="stylesheet" href="/site/css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="/site/css/main.css" type="text/css" media="all" />
<link rel="stylesheet" href="/site/css/blog.css" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Leckerli+One" rel="stylesheet">
<!--// css -->
<!-- font-awesome icons -->
<link href="/site/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- gallery -->
<link rel="stylesheet" href="/site/css/lightbox.css">
<!-- //gallery -->
<!-- font -->
<link href="https://fonts.googleapis.com/css?family=Kaushan+Script&amp;subset=latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="/site/js/jquery-1.11.1.min.js"></script>
<script src="/site/js/bootstrap.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
</head>
<body>
	@include('site.header')
	@yield('content')	
	@include('site.footer')

	<script src="/site/js/responsiveslides.min.js"></script>
	<script src="/site/js/SmoothScroll.min.js"></script>
	{{-- <script type="text/javascript" src="/site/js/move-top.js"></script> --}}
	<script type="text/javascript" src="/site/js/easing.js"></script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {

			// exibe o botão quando chega a um nivel de scroll
			$(window).scroll(function() {

				if ($(this).scrollTop() > 720) {
					$('.to-top').show();
				} else {	
					$('.to-top').hide();
				}
			});
			// botão voltar ao todo
			$('.to-top').on('click', function(e) {
				e.preventDefault();
				$('html, body').animate({
					scrollTop: 0
				});
			});
		});
	</script>
<!-- //here ends scrolling icon -->

{{-- botão curtir facebook --}}
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=1081077611998925";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
{{-- fim botão curtir facebook --}}
</body>	
</html>