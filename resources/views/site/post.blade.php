@extends('site.site')
@section('title', $title)
@section('content')
<div class="about" id="blog">
<a name='blog'></a>
<div class="w3-about-top">
<!-- <h3>V</h3> -->
</div>
<div class="w3l-about">
<div class="container">

<div class="row">
<a name='posts'></a>
<!-- left -->
<section class="col-md-9" class="l-post">

<div id="post-container">
<article class="l-post__item">
<header class="l-post__header">
<h2 class="m-post__title">{{$post->title}}</h2>
<time class="m-post__date">
Publicado em {{timestamp($post->created_at, 'EUA')->format('d/m/Y H:i')}} - <span>Última atualização {{timestamp($post->updated_at, 'EUA')->format('d/m/Y H:i')}}</span>
</time>
<span class="m-post__views text-muted">{{$post->views}} Visualizações</span> 
</header>
<img src="{{ asset('upload/post/' . $post->image) }}" alt="{{$post->title}}" class="img-responsive m-post__thumb">
<hr>
<p class="m-post__content lead">
<?= $post->content; ?>
</p>
<hr>
<h4>Compartilhe</h4>
<p>
<div class="fb-share-button" data-href="{{ url()->current() }}" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Compartilhar</a></div>
</p>

<p>
<a href="https://twitter.com/share?ref_src={{url()->current()}}" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
</article> 
</div>

</section> <!-- ./end-left -->

<!-- right -->
<aside class="col-md-3">
<form id="search" action="/blog" name="s">
<div class="input-group">
<input type="search" class="form-control" name="s" id="search_input" placeholder="Pesquisar">
<span class="input-group-addon">
<span class="glyphicon glyphicon-search"></span>
</span>
</div>
</form>

<div class="form-group maislidas">
<h3 class="text-muted">Mais Lidos</h3>
<ul class="posts-list">
@forelse ($views as $v)
<li>
<a href="{{ route('blogsite.post', ['id' => $v->slug]) }}#posts">{{ $v->title }}</a>
</li>
@empty
<li>Nenhum Post</li>
@endforelse
</ul>
</div>

<div class="form-group recents">
<h3 class="text-muted">Últimos Posts</h3>
<ul class="posts-list">
@forelse ($recents as $r)
<li>
<a href="{{ route('blogsite.post', ['id' => $r->slug]) }}#posts">{{ $r->title }}</a>
</li>
@empty
<li>Nenhum Post</li>
@endforelse
</ul>
</div>
</aside><!-- ./end-right -->
</div>


<div class="col-md-12">
<hr>

<h3>Comentários</h3>
<div class="fb-comments" data-href="{{ url()->current() }}" data-numposts="5"></div>
</div>

</div>
</div>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.12&appId=213131789193195&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script src="{{ asset('site/js/post.js') }}"></script>
@endsection