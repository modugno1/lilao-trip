<header class="header-top">
		<div class="container">
			<section class="row">
				<div class="col-md-6">
					<p class="header-top_contato">
						<i class="fa fa-phone" aria-hidden="true"></i>
						(11) 2985-6681 - 
						<i class="fa fa-whatsapp" aria-hidden="true"></i>
						(11) 94045-2281 | Seg. à Sex. das 9h às 18h
					</p>
				</div>
				<div class="col-md-6">
					<ul class="social">
						<li>
						    <a href="https://www.facebook.com/lilaotrip" target="_blank">
								<img src="/site/images/facebook.svg" alt="Facebook">
							</a>
						</li>
						<li>
							<a href="http://instagram.com/lilaotrip" target="_blank">
								<img src="/site/images/instagram.svg" alt="Instagram">
							</a>
						</li>
						<li>
							<a href="https://www.youtube.com/channel/UCDR-c3zWYDGbDPRd-lqR5gA" target="_blank">
								<img src="/site/images/youtube.svg" alt="Youtube">
							</a>
						</li>
					</ul>
				</div>
			</section>
		</div>
	</header>
	<div class="banner-top">
		<div class="slider">
			<div class="callbacks_container">
				<ul class="rslides callbacks callbacks1" id="slider4">
					@forelse ($banners as $banner)
					<li>
						<div style="background-image: url({{ asset('upload/banner/' . $banner->image) }})" class="w3layouts-banner-generic">
							<div class="container">
								<div class="agileits-banner-info">
									<h3></h3>
									<h4>{{$banner->description}}</h4>
								</div>	
							</div>
						</div>
					</li>
					@empty
					<li>Nenhum banner cadastrado</li>
					@endforelse
				</ul>
			</div>
			<div class="clearfix"> </div>
			<script src="/site/js/responsiveslides.min.js"></script>
			<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						  // Slideshow 4
						  $("#slider4").responsiveSlides({
							auto: true,
							pager:true,
							nav:false,
							speed: 500,
							namespace: "callbacks",
							before: function () {
							  $('.events').append("<li>before event fired.</li>");
							},
							after: function () {
							  $('.events').append("<li>after event fired.</li>");
							}
						  });
					
						});
			</script>
			<!--banner Slider starts Here-->
		</div>
	</div>
	<!-- banner -->
	<div class="banner">
		<div class="header">
			<div class="container">
				<div class="header-center">
					<div class="w3layouts-logo">
						<h1>
							<a href="/">
								<img src="/site/images/logo.png" alt="Lilão Trip" class="logo">
							</a>
						</h1>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="header-bottom">
			<div class="container">
				<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav">
								@if ($menu == 'scroll')
									<li><a class="active list-border" href="/">Início</a></li>
									<li><a href="#quemsomos" class="scroll">Quem Somos</a></li>
									<li><a href="#viagens" class="scroll">Viagens</a></li>
									<li><a href="#fotos" class="scroll">Fotos</a></li>
									<li><a href="#depoimentos" class="scroll">Depoimentos</a></li>
									<li><a href="#contato" class="scroll">Contato</a></li>
									<li><a href="/blog#posts">Blog</a></li>
								@else
									<li><a class="active list-border" href="/">Início</a></li>
									<li><a href="/#quemsomos" class="">Quem Somos</a></li>
									<li><a href="/#viagens" class="">Viagens</a></li>
									<li><a href="/#fotos" class="">Fotos</a></li>
									<li><a href="/#depoimentos" class="">Depoimentos</a></li>
									<li><a href="#contato" class="scroll">Contato</a></li>
									<li><a href="/blog#posts">Blog</a></li>
								@endif
								</ul>	
								<div class="clearfix"> </div>
							</div>	
						</nav>		
				</div>
			</div>
		</div>
	</div>
	<!-- //banner -->