@extends('dashboard.layout')

@section('title', 'Fotos')
@section('subtitle', 'Listagem')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">@yield('subtitle')</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  
    <div class="row">
    <form method="" action=""> {{-- formulario --}}
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            <a href="{{ route('photo.new') }}" class="btn btn-primary">
              <i class="fa fa-plus" aria-hidden="true"></i> Adicionar
            </a>

          </div>
          {{-- fim header box --}}
          
          {{-- mensagens --}}
          @include('dashboard.components.mensagem')

          {{-- box content --}}
         
          <div class="box-body pad row">
            
            {{-- deixa a tabela responsiva --}}
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Imagem</th>
                    <th>Ações</th>
                  </tr>
                </thead>

                <tbody>
                @forelse ($photos as $photo)
                  <tr>
                    <td>{{ $photo->id }}</td>
                    <td>
                      @if (empty($photo->url))
                      <img src="http://fakeimg.pl/150x150?text=Sem Foto" class="img-ronded" style="height: 80px">
                      @else
                        <img src="{{ asset('upload/gallery/' . $photo->url) }}" class="img-ronded" style="height: 80px">
                      @endif
                    </td>
                    <td>
                        {{-- btn remover --}}
                        <a href="{{ route('photo.remove', ['id' => $photo->id]) }}" class="btn btn-danger btn-sm" title="Remover" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </td>
                  </tr>
                @empty
                <h4>Nenhuma foto encontrada</h4>
                @endforelse
                </tbody>

              </table>
            </div>
            {{-- fim table-responsive --}}
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>
    
    <!-- paginação -->
    <div class="row">
      <div class="col-md-12">
        {{ $photos->links() }}
      </div>
    </div>
    <!-- ./row -->
  </section>

@endsection