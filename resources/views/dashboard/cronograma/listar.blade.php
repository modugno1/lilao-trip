@extends('dashboard.layout')

@section('title', 'Cronogramas')
@section('subtitle', 'Listagem')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">@yield('subtitle')</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form method="" action=""> {{-- formulario --}}
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
          <a href="{{ url('admin/viagem') }}" class="btn btn-default">Voltar</a>
            <a href="{{ url('admin/cronograma/novo', ['viagem_id' => $viagem_id]) }}" class="btn btn-primary">
              <i class="fa fa-plus" aria-hidden="true"></i> Adicionar
            </a>

          </div>
          {{-- fim header box --}}
          
          {{-- mensagens --}}
          @include('dashboard.components.mensagem')

          {{-- box content --}}
          <div class="box-body pad row">
            
            {{-- btn todos --}}
            @if (isset($num_pesquisa))
              <div class="col-md-12">
                <a href="{{ url('admin/cronograma', ['viagem_id' => $viagem_id]) }}" class="btn btn-default">
                  <i class="fa fa-th-list" aria-hidden="true"></i> Todos
                </a>
              </div>
            @endif
            {{-- fim btn todos --}}

            {{-- deixa a tabela responsiva --}}
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Viagem</th>
                    <th>Ações</th>
                  </tr>
                </thead>

                <tbody>
                @forelse ($cronogramas as $cronograma)
                  <tr>
                    <td>{{ $cronograma->id }}</td>
                    <td>{{ $cronograma->titulo }}</td>
                    <td>{{ $cronograma->viagem->titulo }}</td>
                    <td>
                        {{-- btn editar --}}
                        <a href="{{ url('admin/cronograma/editar', ['id' => $cronograma->id]) }}" class="btn btn-success btn-sm" title="Editar" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>

                        {{-- btn remover --}}
                        <a href="{{ url('admin/cronograma/remover', ['id' => $cronograma->id]) }}" class="btn btn-danger btn-sm" title="Remover" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </td>
                  </tr>
                @empty
                <h4>Nenhuma Viagem encontrada</h4>
                @endforelse
                </tbody>

              </table>
            </div>
            {{-- fim table-responsive --}}
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>

@endsection