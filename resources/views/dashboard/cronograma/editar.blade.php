@extends('dashboard.layout')

@section('title', 'Cronograma')
@section('subtitle', 'Editar')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">@yield('title')</a></li>
      <li class="active">@yield('subtitle')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form id="form-submit" method="POST" action="{{ url('admin/cronograma/atualizar') }}"> {{-- formulario --}}
    {!! csrf_field() !!}
    <input type="hidden" name="id" value="{{  $cronograma->id }}">
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            {{--btn fechar/minimizar --}}
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
            {{-- fim btn fechar/minimizar --}}

          </div>
          {{-- fim header box --}}

          {{-- mensagens --}}
          @include('dashboard.components.mensagem')

          {{-- box content --}}
          <div class="box-body pad row">
              <div class="col-md-12">
                <a href="{{ url('admin/cronograma', ['viagem_id' => $cronograma->viagem_id]) }}" class="btn btn-default">
                  Voltar
                </a>
                <hr>
              </div>
              
              <div class="form-group col-md-4">
                <label for="titulo">Título</label>
                <input type="text" class="form-control" name="titulo" id="titulo" placeholder="Título" value="{{ $cronograma->titulo }}" required>
              </div>

              <div class="form-group col-md-12">
                <label for="descricao">Descrição</label>
                <textarea name="descricao" id="descricao" cols="30" rows="10" class="bs-editor form-control">{{ $cronograma->descricao }}</textarea>
              </div>
              

              {{-- botões salvar --}}
              <div class="form-group col-md-2 pull-right">
                  <button type="submit" class="btn btn-primary btn-save btn-block pull-right">
                    <span>Salvar</span> 
                     <img src="/dashboard/assets/img/icones/loading-2-sm.svg" class="btn-load-img">
                  </button>
              </div>
              
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>
<script>
  // Bootstrap Editor
  $(document).ready(function() {
    $('.bs-editor').wysihtml5();
  });
</script>
@endsection