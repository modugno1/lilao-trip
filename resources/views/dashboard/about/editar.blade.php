@extends('dashboard.layout')

@section('title', 'Quem Somos')
@section('subtitle', 'Editar')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
<h1>
@yield('title')
<small>@yield('subtitle')</small>
</h1>
<ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="#">@yield('title')</a></li>
<li class="active">@yield('subtitle')</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
<form id="form-submit" method="POST" action="{{ route('about.update', ['id' => $about->id]) }}" enctype="multipart/form-data"> {{-- formulario --}}
{!! csrf_field() !!}
{{-- box-pai --}}
<section class="col-md-12">
{{-- box --}}
<section class="box">

{{-- fim header box --}}

{{-- mensagens --}}
@include('dashboard.components.mensagem')

{{-- box content --}}
<div class="box-body pad row">
<div class="col-md-12">
<hr>
</div>
<div class="form-group col-md-12">
<div class="form-group">
<label for="title_about">Primeiro Título</label>
<input type="text" class="form-control" name="title_about" id="title_about" placeholder="Primeiro Título" value="{{ $about->title_about }}">
</div>
<div class="form-group">
<label for="content_about">Primeiro Conteúdo</label>
<textarea class="form-control" name="content_about" id="content_about" rows="10" placeholder="Primeiro Conteúdo">{{ $about->content_about }}</textarea>
</div>
<div class="form-group">
<label for="title_why">Segundo Título</label>
<input type="text" class="form-control" name="title_why" id="title_why" placeholder="Segundo Título" value="{{ $about->title_why }}">
</div>
<div class="form-group">
<label for="content_why">Segundo Conteúdo</label>
<textarea class="form-control" name="content_why" id="content_why" rows="10" placeholder="Segundo Conteúdo">{{ $about->content_why }}</textarea>
</div>
<div class="col-md-6">
<div class="form-group">
<label for="video_one">Vídeo Esquerda (Hash do YOUTUBE, ex: fHiWiVvAuEY)</label>
<input type="text" class="form-control" name="video_one" id="video_one" placeholder="Ex: fHiWiVvAuEY" value="{{ $about->video_one }}">
</div>
</div>
<div class="col-md-6">
<div class="form-group">
<label for="video_two">Vídeo Direita (Hash do YOUTUBE, ex: fHiWiVvAuEY)</label>
<input type="text" class="form-control" name="video_two" id="video_two" placeholder="Ex: fHiWiVvAuEY" value="{{ $about->video_two }}">
</div>
</div>
<div class="form-group">
<button type="submit" class="btn btn-primary btn-save pull-right">
<span>Atualizar</span> 
<img src="/dashboard/assets/img/icones/loading-2-sm.svg" class="btn-load-img">
</button>
</div>
</div>
{{-- botões salvar --}}


</div>
{{-- fim content --}}
</section>
{{-- fim box --}}
</section>
{{-- fim box-pai --}}

</form> {{-- fim formulario --}}
<!-- /.col-->
</div>
<!-- ./row -->
</section>

@endsection