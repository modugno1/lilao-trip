<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left user-initial-name">
            <h1>{{ str_initial( Auth::user()->name , '', false) }}</h1>
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="{{ url('admin/usuario/editar', ['id' => Auth::user()->id]) }}"><i class="fa fa-edit"></i> Editar Perfil</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Menu</li>
        <li>
          <a href="{{ url('admin/viagem') }}">
            <i class="fa fa-circle-o text-red"></i> <span>Viagem</span>
          </a>
        </li>
        <li>
          <a href="{{ route('about.edit') }}">
            <i class="fa fa-circle-o text-red"></i> <span>Quem Somos</span>
          </a>
        </li>
        <li>
          <a href="{{ route('banner') }}">
            <i class="fa fa-circle-o text-red"></i> <span>Banners</span>
          </a>
        </li>
        <li>
          <a href="{{ route('photo') }}">
            <i class="fa fa-circle-o text-red"></i> <span>Fotos</span>
          </a>
        </li>
        <li>
          <a href="{{ route('depoimento') }}">
            <i class="fa fa-circle-o text-red"></i> <span>Depoimentos</span>
          </a>
        </li>
        <li>
          <a href="{{ route('post') }}">
            <i class="fa fa-circle-o text-red"></i> <span>Blog</span>
          </a>
        </li>
        
        <li>
          <a href="{{ url('admin/manutencao') }}">
            <i class="fa fa-circle-o text-blue"></i> <span>Manutenção</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>