{{-- Mensagens de Validação --}}
@if (count($errors) > 0)
	<div class="col-md-12">
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<ul style="padding-left: 1em">
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>
@endif
{{-- Mensagens do Sistema --}}
@if (Session::has('msg-warning'))
	<div class="col-md-12">
		<div class="alert alert-warning">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			{{ Session::get('msg-warning') }}
		</div>
	</div>
@endif

@if (Session::has('msg-info'))
	<div class="col-md-12">
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			{{ Session::get('msg-info') }}
		</div>
	</div>
@endif

@if (Session::has('msg-ok'))
	<div class="col-md-12">
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			{{ Session::get('msg-ok') }}
		</div>
	</div>
@endif

@if (Session::has('msg-error'))
	<div class="col-md-12">
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			{{ Session::get('msg-error') }}
		</div>
	</div>
@endif

@if (isset($num_pesquisa))
	<div class="col-md-12">
		<div class="alert alert-info" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			@if ( isset($pesquisa) )
				<strong>{{ $num_pesquisa }} Resultado(s) para: '{{ $pesquisa }}'</strong>
			@else
				<strong>{{ $num_pesquisa }} Resultado(s)</strong>
			@endif
		</div>
	</div>
@endif