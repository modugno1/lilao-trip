@extends('dashboard.layout')

@section('title', 'Fragmentos')
@section('subtitle', 'Novo')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form method="" action=""> {{-- formulario --}}
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            <h3 class="box-title">Título
              <small>Subtitulo</small>
            </h3>

            {{--btn fechar/minimizar --}}
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
            {{-- fim btn fechar/minimizar --}}

          </div>
          {{-- fim header box --}}
          
          {{-- box content --}}
          <div class="box-body pad row">
            

              <div class="form-group col-md-4">
                <label for="campo1">Campo 1</label>
                <input type="text" class="form-control" name="campo1" id="campo1">
              </div>
              <div class="form-group col-md-4">
                <label for="campo1">Campo 1</label>
                <input type="text" class="form-control" name="campo1" id="campo1">
              </div>
              <div class="form-group col-md-4">
                <label for="campo1">Campo 1</label>
                <input type="text" class="form-control" name="campo1" id="campo1">
              </div>

              <div class="form-group col-md-12">
                <textarea class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
              
              {{-- botões salvar --}}
              <div class="form-group col-md-2 pull-right">
                  <a href="#" class="btn btn-primary btn-load btn-save btn-block pull-right">
                    <span>Salvar</span> 
                     <img src="/dashboard/assets/img/icones/loading-2-sm.svg" class="btn-load-img">
                  </a>
              </div>

              {{-- botões remover --}}
              <div class="form-group col-md-2 pull-left">
                  <a href="#" class="btn btn-danger btn-load btn-remover btn-block">
                    Remover
                  </a>
              </div>
            
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>
    
@endsection