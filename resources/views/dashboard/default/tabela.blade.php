@extends('dashboard.layout')

@section('title', 'Fragmentos')
@section('subtitle', 'Listagem')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Listagem</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form method="" action=""> {{-- formulario --}}
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            <h3 class="box-title">Título
              <small>Subtitulo</small>
            </h3>

            {{--btn fechar/minimizar --}}
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
            {{-- fim btn fechar/minimizar --}}

          </div>
          {{-- fim header box --}}
          
          {{-- box content --}}
          <div class="box-body pad row">
            
            {{-- botões superiores --}}
            <div class="col-sm-6">
              <a href="#" class="btn btn-primary">
                <i class="fa fa-plus" aria-hidden="true"></i> Adicionar
              </a>
            </div>

            {{-- campo de busca --}}
            <div class="col-sm-6">
              <form action="" method="POST">
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Pesquisar" name="">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-default">Pesquisar</button>
                    </span>
                  </div>
                </div>
              </form>
            </div>
            
            {{-- deixa a tabela responsiva --}}
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Descrição</th>
                    <th>Ações</th>
                  </tr>
                </thead>

                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Lorem ipsum dolor sit.</td>
                    <td>
                        {{-- btn editar --}}
                        <a href="" class="btn btn-success btn-sm" title="Editar" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>

                        {{-- btn remover --}}
                        <a href="" class="btn btn-danger btn-sm" title="Remover" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </td>
                  </tr>
                </tbody>

              </table>
            </div>
            {{-- fim table-responsive --}}
            
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>
    
@endsection