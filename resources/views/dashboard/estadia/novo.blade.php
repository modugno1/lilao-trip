@extends('dashboard.layout')

@section('title', 'Estadia')
@section('subtitle', 'Novo')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">@yield('title')</a></li>
      <li class="active">@yield('subtitle')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form id="form-submit" method="POST" action="{{ url('admin/estadia/cadastrar') }}"> {{-- formulario --}}
    {!! csrf_field() !!}
    <input type="hidden" name="viagem_id" value="{{  $viagem_id }}">
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            {{--btn fechar/minimizar --}}
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
            {{-- fim btn fechar/minimizar --}}

          </div>
          {{-- fim header box --}}

          {{-- mensagens --}}
          @include('dashboard.components.mensagem')

          {{-- box content --}}
          <div class="box-body pad row">
              <div class="col-md-12">
                <a href="{{ url('admin/estadia', ['viagem_id' => $viagem_id]) }}" class="btn btn-default">
                  Voltar
                </a>
                <hr>
              </div>
              
              <div class="form-group col-md-4">
                <label for="tipo">Tipo</label>
                <input type="text" class="form-control" name="tipo" id="tipo" placeholder="Tipo" value="{{ old('tipo') }}" required>
              </div>

              <div class="form-group col-md-4">
                <label for="valor">Preço</label>
                <input type="text" class="form-control" name="valor" id="valor" placeholder="Preço" value="{{ old('valor') }}" required>
              </div>

              <div class="form-group col-md-4">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                  <option value="1">Disponível</option>
                  <option value="0">Indisponível</option>
                </select>
              </div>
              <div class="form-group col-md-12">
                <label for="pagseguro">Pagseguro</label>
                <input type="text" class="form-control" name="pagseguro" id="pagseguro" placeholder="Pagseguro" value="{{ old('pagseguro') }}" required>
              </div>

              

              {{-- botões salvar --}}
              <div class="form-group col-md-2 pull-right">
                  <button type="submit" class="btn btn-primary btn-save btn-block pull-right">
                    <span>Salvar</span> 
                     <img src="/dashboard/assets/img/icones/loading-2-sm.svg" class="btn-load-img">
                  </button>
              </div>
              
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>

@endsection