{{-- Modal Remover --}}
<div class="modal fade bs-example-modal-sm" id="remover" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Remover Registro</h4>
      </div>
      <div class="modal-body">
        <p class="text-center">Deseja remover esse registro?</p>
      </div>
      <div class="modal-footer">
      <form action="{{ url('fragment/usuario/remover') }}" method="POST">
      	{!! csrf_field() !!}
      	<input type="hidden" name="id" id="id">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-danger">Remover</button>
      </form>
      </div>
    </div>
  </div>
</div>