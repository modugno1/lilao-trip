@extends('dashboard.layout')

@section('title', 'Usuários')
@section('subtitle', 'Listagem')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">@yield('subtitle')</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form method="" action=""> {{-- formulario --}}
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            <a href="{{ url('fragment/usuario/novo') }}" class="btn btn-primary">
              <i class="fa fa-plus" aria-hidden="true"></i> Adicionar
            </a>

            {{-- campo de busca --}}
            <div class="col-sm-6 pull-right">
              <form action="{{ url('usuarios/pesquisar') }}" method="GET">
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Pesquisar por Nome ou E-mail" name="pesquisar">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-default">Pesquisar</button>
                    </span>
                  </div>
                </div>
              </form>
            </div>

          </div>
          {{-- fim header box --}}
          
          {{-- mensagens --}}
          @include('dashboard.components.mensagem')

          {{-- box content --}}
          <div class="box-body pad row">
            
            {{-- btn todos --}}
            @if (isset($num_pesquisa))
              <div class="col-md-12">
                <a href="{{ url('fragment/usuarios') }}" class="btn btn-default">
                  <i class="fa fa-th-list" aria-hidden="true"></i> Todos
                </a>
              </div>
            @endif
            {{-- fim btn todos --}}

            {{-- deixa a tabela responsiva --}}
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Ações</th>
                  </tr>
                </thead>

                <tbody>
                @foreach ($usuarios as $usu)
                  <tr>
                    <td>{{ $usu->id }}</td>
                    <td>{{ $usu->name }}</td>
                    <td>{{ $usu->email }}</td>
                    <td>
                        {{-- btn editar --}}
                        <a href="{{ url('fragment/usuario/editar', ['id' => $usu->id]) }}" class="btn btn-success btn-sm" title="Editar" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>

                        {{-- btn remover --}}
                        <a href="#" data-id="{{ $usu->id }}" class="btn btn-danger btn-sm remover" title="Remover" data-toggle="modal" data-target="#remover">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </td>
                  </tr>
                @endforeach
                </tbody>

              </table>
            </div>
            {{-- fim table-responsive --}}
          
          {{-- paginação --}}
          <div class="col-md-12 text-center">
            @if (isset($filtros))
              {{ $usuarios->appends($filtros)->links() }}
            @else
              {{ $usuarios->links() }}
            @endif
          </div>
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>

{{-- scripts da página --}}
<script src="/dashboard/assets/js/usuario.js"></script>

{{-- includes --}}
@include('dashboard.usuario.modal.remover')

@endsection