@extends('dashboard.layout')

@section('title', 'Usuário')
@section('subtitle', isset($user->name) ? $user->name : 'Novo')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">@yield('title')</a></li>
      <li class="active">@yield('subtitle')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form id="form-submit" method="POST" action="{{ url($form_url) }}"> {{-- formulario --}}
    {!! csrf_field() !!}
    @if (isset($user->id))
      <input type="hidden" name="id" value="{{ $user->id }}">
    @endif
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            {{--btn fechar/minimizar --}}
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
            {{-- fim btn fechar/minimizar --}}

          </div>
          {{-- fim header box --}}

          {{-- mensagens --}}
          @include('dashboard.components.mensagem')

          {{-- box content --}}
          <div class="box-body pad row">

              <div class="form-group col-md-4">
                <label for="name">Nome</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Nome" value="{{ isset($user) ? $user->name : old('name') }}">
              </div>
              <div class="form-group col-md-4">
                <label for="email">E-mail</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="E-mail" value="{{ isset($user) ? $user->email : old('email') }}">
              </div>
              {{-- se for tela de edição, coloca campo 'nova senha' --}}
              @if (isset($user))
                <div class="form-group col-md-2">
                  <label for="password_old">Senha Antiga</label>
                  <input type="password" class="form-control password" name="password_old" id="password_old" placeholder="Senha Antiga">
                  <label>
                    <input type="checkbox" class="mostrar-senha"> Mostrar Senha
                  </label>
                </div>
                <div class="form-group col-md-2">
                  <label for="password_new">Nova Senha</label>
                  <input type="password" class="form-control password" name="password_new" id="password_new" placeholder="Nova Senha">
                  <label>
                    <input type="checkbox" class="mostrar-senha"> Mostrar Senha
                  </label>
                </div>
              @else
                <div class="form-group col-md-4">
                  <label for="password">Senha</label>
                  <input type="password" class="form-control password" name="password" id="password" placeholder="Senha">
                  <label>
                    <input type="checkbox" class="mostrar-senha"> Mostrar Senha
                  </label>
                </div>
              @endif

              {{-- botões salvar --}}
              <div class="form-group col-md-2 pull-right">
                  <button type="submit" class="btn btn-primary btn-save btn-block pull-right">
                    <span>Salvar</span> 
                     <img src="/dashboard/assets/img/icones/loading-2-sm.svg" class="btn-load-img">
                  </button>
              </div>
              
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>

{{-- scripts da página --}}
<script src="/dashboard/assets/js/usuario.js"></script>
{{-- includes --}}
@include('dashboard.usuario.modal.remover')
@endsection