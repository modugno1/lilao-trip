@extends('dashboard.layout')

@section('title', 'Depoimentos')
@section('subtitle', 'Listagem')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">@yield('subtitle')</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form method="" action=""> {{-- formulario --}}
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            <a href="{{ route('depoimento.new') }}" class="btn btn-primary">
              <i class="fa fa-plus" aria-hidden="true"></i> Adicionar
            </a>

          </div>
          {{-- fim header box --}}
          
          {{-- mensagens --}}
          @include('dashboard.components.mensagem')

          {{-- box content --}}
          <div class="box-body pad row">
            
            {{-- deixa a tabela responsiva --}}
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Data de Publicação</th>
                    <th>Ações</th>
                  </tr>
                </thead>

                <tbody>
                @forelse ($depoimentos as $depo)
                  <tr>
                    <td>{{ $depo->id }}</td>
                    <td>{{ $depo->name }}</td>
                    <td>{{ $depo->date }}</td>
                    <td>
                        {{-- btn editar --}}
                        <a href="{{ route('depoimento.edit', ['id' => $depo->id]) }}" class="btn btn-success btn-sm" title="Editar" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>

                        {{-- btn remover --}}
                        <a href="{{ route('depoimento.remove', ['id' => $depo->id]) }}" class="btn btn-danger btn-sm" title="Remover" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </td>
                  </tr>
                @empty
                <h4>Nenhum Depoimento encontrado</h4>
                @endforelse
                </tbody>

              </table>
            </div>
            {{-- fim table-responsive --}}
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>

    <!-- paginação -->
    <div class="row">
      <div class="col-md-12">
        {{ $depoimentos->links() }}
      </div>
    </div>

    <!-- ./row -->
  </section>

@endsection