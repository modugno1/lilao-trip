@extends('dashboard.layout')

@section('title', 'Depoimento')
@section('subtitle', 'Editar')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
<h1>
@yield('title')
<small>@yield('subtitle')</small>
</h1>
<ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="#">@yield('title')</a></li>
<li class="active">@yield('subtitle')</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
<form id="form-submit" method="POST" action="{{ route('depoimento.update', ['id' => $depoimento->id]) }}" enctype="multipart/form-data"> {{-- formulario --}}
{!! csrf_field() !!}
{{-- box-pai --}}
<section class="col-md-12">
{{-- box --}}
<section class="box">

{{-- fim header box --}}

{{-- mensagens --}}
@include('dashboard.components.mensagem')

{{-- box content --}}
<div class="box-body pad row">
<div class="col-md-12">
<a href="{{ route('depoimento') }}" class="btn btn-default">
Voltar
</a>
<hr>
</div>
<div class="form-group col-md-12">
<div class="form-group">
<label for="name">Nome do Autor</label>
<input type="text" class="form-control" name="name" id="name" placeholder="Título" value="{{ $depoimento->name }}">
</div>
<div class="form-group">
<label for="date">Data</label>
<input type="text" class="form-control" name="date" id="date" placeholder="Data" value="{{ $depoimento->date }}">
</div>
<div class="form-group">
<label for="content">Depoimento</label>
<textarea class="form-control" name="content" id="content" rows="10" placeholder="Conteúdo">{{ $depoimento->content }}</textarea>
</div>
<div class="form-group">
<button type="submit" class="btn btn-primary btn-save pull-right">
<span>Atualizar</span> 
<img src="/dashboard/assets/img/icones/loading-2-sm.svg" class="btn-load-img">
</button>
</div>
</div>
{{-- botões salvar --}}


</div>
{{-- fim content --}}
</section>
{{-- fim box --}}
</section>
{{-- fim box-pai --}}

</form> {{-- fim formulario --}}
<!-- /.col-->
</div>
<!-- ./row -->
</section>

@endsection