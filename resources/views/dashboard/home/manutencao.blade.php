@extends('dashboard.layout')

@section('title', 'Manutenção')
@section('subtitle', 'Status')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">@yield('title')</a></li>
      <li class="active">@yield('subtitle')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form id="form-submit" method="POST" action="{{ url('admin/manutencao/cadastrar') }}" enctype="multipart/form-data"> {{-- formulario --}}
    {!! csrf_field() !!}
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            {{--btn fechar/minimizar --}}
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
            {{-- fim btn fechar/minimizar --}}

          </div>
          {{-- fim header box --}}

          {{-- mensagens --}}
          @include('dashboard.components.mensagem')

          {{-- box content --}}
          <div class="box-body pad row">
              
              <div class="form-group col-md-4">
                <label for="manutencao">Ambiente</label>
                <select name="manutencao" id="manutencao" class="form-control">
                  <option value="1" {{ $manutencao->manutencao == 1 ? 'selected' : '' }}>Manutenção</option>
                  <option value="0" {{ $manutencao->manutencao == 0 ? 'selected' : '' }}>Produção</option>
                </select>
              </div>
          
              {{-- botões salvar --}}
              <div class="form-group col-md-2 pull-right">
                  <button type="submit" class="btn btn-primary btn-save btn-block pull-right">
                    <span>Salvar</span> 
                     <img src="/dashboard/assets/img/icones/loading-2-sm.svg" class="btn-load-img">
                  </button>
              </div>
              
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>

@endsection