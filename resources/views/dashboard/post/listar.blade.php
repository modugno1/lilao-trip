@extends('dashboard.layout')

@section('title', 'Posts')
@section('subtitle', 'Listagem')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">@yield('subtitle')</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form method="" action=""> {{-- formulario --}}
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            <a href="{{ route('post.new') }}" class="btn btn-primary">
              <i class="fa fa-plus" aria-hidden="true"></i> Adicionar
            </a>

          </div>
          {{-- fim header box --}}
          
          {{-- mensagens --}}
          @include('dashboard.components.mensagem')

          {{-- box content --}}
          <div class="box-body pad row">
            
            {{-- deixa a tabela responsiva --}}
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Imagem</th>
                    <th>Título</th>
                    <th>Data de Publicação</th>
                    <th>Data Última Alteração</th>
                    <th>Ações</th>
                  </tr>
                </thead>

                <tbody>
                @forelse ($posts as $post)
                  <tr>
                    <td>{{ $post->id }}</td>
                    <td>
                      @if (empty($post->image))
                      <img src="https://fakeimg.pl/150x150?text=Sem Foto" class="img-ronded" style="height: 80px">
                      @else
                        <img src="{{ asset('upload/post/' . $post->image) }}" class="img-ronded" style="height: 80px">
                      @endif
                    </td>
                    <td>{{ $post->title }}</td>
                    <td>{{ timestamp($post->created_at, 'EUA')->format('d/m/Y H:i') }}</td>
                    <td>{{ timestamp($post->updated_at, 'EUA')->format('d/m/Y H:i') }}</td>
                    <td>
                        {{-- btn editar --}}
                        <a href="{{ route('post.edit', ['id' => $post->id]) }}" class="btn btn-success btn-sm" title="Editar" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>

                        {{-- btn remover --}}
                        <a href="{{ route('post.remove', ['id' => $post->id]) }}" class="btn btn-danger btn-sm" title="Remover" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </td>
                  </tr>
                @empty
                <h4>Nenhum Post encontrado</h4>
                @endforelse
                </tbody>

              </table>
            </div>
            {{-- fim table-responsive --}}
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>

    <div class="row">
      <div class="col-md-12">
        {{ $posts->links() }}
      </div>
    </div>
    <!-- ./row -->
  </section>

@endsection