@extends('dashboard.layout')

@section('title', 'Post')
@section('subtitle', 'Editar')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
<h1>
@yield('title')
<small>@yield('subtitle')</small>
</h1>
<ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="#">@yield('title')</a></li>
<li class="active">@yield('subtitle')</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
<form id="form-submit" method="POST" action="{{ route('post.update', ['id' => $post->id]) }}" enctype="multipart/form-data"> {{-- formulario --}}
{!! csrf_field() !!}
{{-- box-pai --}}
<section class="col-md-12">
{{-- box --}}
<section class="box">

{{-- fim header box --}}

{{-- mensagens --}}
@include('dashboard.components.mensagem')

{{-- box content --}}
<div class="box-body pad row">
<div class="col-md-12">
<a href="{{ route('post') }}" class="btn btn-default">
Voltar
</a>
<hr>
</div>
<div class="form-group col-md-9">
<div class="form-group">
<label for="title">Título</label>
<input type="text" class="form-control" name="title" id="title" placeholder="Título" value="{{ $post->title }}">
</div>
<div class="form-group">
<label for="content">Conteúdo</label>
<textarea class="form-control ckeditor" name="content" id="content" placeholder="Conteúdo">{{ $post->content }}</textarea>
</div>
</div>
<div class="form-group col-md-3">
<div class="form-group">
<label for="titulo">Imagem</label>
<input type="file" name="image">
@if (!empty($post->image))
  <img src="{{ asset('upload/post/' . $post->image) }}" alt="{{$post->title}}" class="img-responsive">
@else
<img src="https://fakeimg.pl/300x240?text=Sem Imagem" alt="{{$post->title}}" class="img-responsive">
@endif
</div>
<div class="form-group">
<button type="submit" class="btn btn-primary btn-save btn-block pull-right">
<span>Atualizar</span> 
<img src="/dashboard/assets/img/icones/loading-2-sm.svg" class="btn-load-img">
</button>
</div>
</div>
{{-- botões salvar --}}


</div>
{{-- fim content --}}
</section>
{{-- fim box --}}
</section>
{{-- fim box-pai --}}

</form> {{-- fim formulario --}}
<!-- /.col-->
</div>
<!-- ./row -->
</section>

<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
<script>
	CKEDITOR.replace('.ckeditor');
</script>
@endsection