@extends('dashboard.layout')

@section('title', 'Viagens')
@section('subtitle', 'Listagem')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">@yield('subtitle')</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form method="" action=""> {{-- formulario --}}
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            <a href="{{ url('admin/viagem/novo') }}" class="btn btn-primary">
              <i class="fa fa-plus" aria-hidden="true"></i> Adicionar
            </a>

          </div>
          {{-- fim header box --}}
          
          {{-- mensagens --}}
          @include('dashboard.components.mensagem')

          {{-- box content --}}
          <div class="box-body pad row">
            
            {{-- btn todos --}}
            @if (isset($num_pesquisa))
              <div class="col-md-12">
                <a href="{{ url('admin/viagems') }}" class="btn btn-default">
                  <i class="fa fa-th-list" aria-hidden="true"></i> Todos
                </a>
              </div>
            @endif
            {{-- fim btn todos --}}

            {{-- deixa a tabela responsiva --}}
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Imagem</th>
                    <th>Título</th>
                    <th>Estadia</th>
                    <th>Cronograma</th>
                    <th>Ações</th>
                  </tr>
                </thead>

                <tbody>
                @forelse ($viagens as $viagem)
                  <tr>
                    <td>{{ $viagem->id }}</td>
                    <td>
                      @if (empty($viagem->imagem))
                        <a href="{{ url('admin/viagem/' . $viagem->id . '/imagem') }}">Adicionar</a>
                      @else
                        <img src="/upload/{{ $viagem->imagem }}" class="img-ronded" style="height: 80px">
                        <br>
                        <a href="{{ url('admin/viagem/' . $viagem->id . '/imagem') }}">Alterar</a>
                      @endif
                    </td>
                    <td>{{ $viagem->titulo }}</td>
                    <td>
                      <a href="{{ url('admin/estadia', ['viagem_id' => $viagem->id]) }}">Adicionar</a>
                    </td>
                    <td>
                      <a href="{{ url('admin/cronograma', ['id' => $viagem->id]) }}">Adicionar</a>
                    </td>
                    <td>
                        {{-- btn editar --}}
                        <a href="{{ url('admin/viagem/editar', ['id' => $viagem->id]) }}" class="btn btn-success btn-sm" title="Editar" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>

                        {{-- btn remover --}}
                        <a href="{{ url('admin/viagem/remover', ['id' => $viagem->id]) }}" class="btn btn-danger btn-sm" title="Remover" data-toggle="tooltip" data-placement="top">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </td>
                  </tr>
                @empty
                <h4>Nenhuma Viagem encontrada</h4>
                @endforelse
                </tbody>

              </table>
            </div>
            {{-- fim table-responsive --}}
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>

@endsection