@extends('dashboard.layout')

@section('title', 'Viagem')
@section('subtitle', 'Editar')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">@yield('title')</a></li>
      <li class="active">@yield('subtitle')</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    <form id="form-submit" method="POST" action="{{ url('admin/viagem/atualizar') }}"> {{-- formulario --}}
    {!! csrf_field() !!}
    <input type="hidden" name="id" value="{{ $viagem->id }}">
    {{-- box-pai --}}
      <section class="col-md-12">
        {{-- box --}}
        <section class="box">
        
        {{-- header box --}}
          <div class="box-header with-border">
            {{--btn fechar/minimizar --}}
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
            {{-- fim btn fechar/minimizar --}}

          </div>
          {{-- fim header box --}}

          {{-- mensagens --}}
          @include('dashboard.components.mensagem')

          {{-- box content --}}
          <div class="box-body pad row">
              <div class="col-md-12">
                <a href="{{ url('admin/viagem') }}" class="btn btn-default">
                  Voltar
                </a>
                <hr>
              </div>
              <div class="form-group col-md-3">
                <label for="titulo">Título</label>
                <input type="text" class="form-control" name="titulo" id="titulo" placeholder="Título" value="{{ $viagem->titulo }}">
              </div>

              <div class="form-group col-md-3">
                <label for="valor">Valor</label>
                <input type="text" class="form-control" name="valor" id="valor" placeholder="Valor" value="{{ $viagem->valor }}">
              </div>

              <div class="form-group col-md-3">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                  <option value="1" {{ $viagem->status == 1 ? 'selected' : '' }}>Disponível</option>
                  <option value="0" {{ $viagem->status == 0 ? 'selected' : '' }}>Indisponível</option>
                </select>
              </div>

              <div class="form-group col-md-3">
                <label for="saida_retorno">Saída-Retorno</label>
                <input type="text" class="form-control" name="saida_retorno" id="saida_retorno" placeholder="Título" value="{{ $viagem->saida_retorno }}">
              </div>

              <div class="form-group col-md-12">
                <label for="observacao">Observação</label>
                <input type="text" class="form-control" name="observacao" id="observacao" placeholder="Observação" value="{{ $viagem->observacao }}">
              </div>

              <div class="form-group col-md-12">
                <label for="apresentacao">Apresentação <span class="text-muted">(Máx: 1024 Caracteres)</span></label>
                <textarea name="apresentacao" id="apresentacao" cols="30" rows="4" class="form-control">{{ $viagem->apresentacao }}</textarea>
              </div>
              
              <div class="form-group col-md-12">
                <label for="descricao">Descrição</label>
                <textarea name="descricao" id="descricao" cols="30" rows="10" class="bs-editor form-control">{{ $viagem->descricao }}</textarea>
              </div>

              <div class="form-group col-md-12">
                <label for="incluso">O que está incluso?</label>
                <textarea name="incluso" id="incluso" cols="30" rows="10" class="bs-editor form-control">{{ $viagem->incluso }}</textarea>
              </div>

              <div class="form-group col-md-12">
                <label for="levar">O que levar?</label>
                <textarea name="levar" id="levar" cols="30" rows="10" class="bs-editor form-control">{{ $viagem->levar }}</textarea>
              </div>

              <div class="form-group col-md-12">
                <label for="importante">Informações Importantes?</label>
                <textarea name="importante" id="importante" cols="30" rows="10" class="bs-editor form-control">{{ $viagem->importante }}</textarea>
              </div>

              {{-- botões salvar --}}
              <div class="form-group col-md-2 pull-right">
                  <button type="submit" class="btn btn-primary btn-save btn-block pull-right">
                    <span>Salvar</span> 
                     <img src="/dashboard/assets/img/icones/loading-2-sm.svg" class="btn-load-img">
                  </button>
              </div>
              
          </div>
          {{-- fim content --}}
        </section>
        {{-- fim box --}}
      </section>
      {{-- fim box-pai --}}

    </form> {{-- fim formulario --}}
      <!-- /.col-->
    </div>
    <!-- ./row -->
  </section>
<script>
  // Bootstrap Editor
  $(document).ready(function() {
    $('.bs-editor').wysihtml5();
  });
</script>
@endsection