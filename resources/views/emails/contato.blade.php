<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Contato</title>
</head>
<body>
	<h4>Lilão Trip</h4>
	<p><strong>
	@if (isset($primeiro_nome))
		{{ $primeiro_nome }}
	@endif
	@if (isset($segundo_nome))
		{{ ' '.$segundo_nome }}
	@endif
	</strong></p>

	@if (isset($tel))
		<p>
			<strong>Telefone: </strong>{{ $tel }}
		</p>
	@endif
	@if (isset($email))
		<p>
			<strong>E-mail: </strong>{{ $email }}
		</p>
	@endif
	@if (isset($mensagem))
		<p>
			<strong>Mensagem: </strong>{{ $mensagem }}
		</p>
	@endif
</body>
</html>